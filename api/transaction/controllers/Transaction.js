'use strict';

/**
 * Transaction.js controller
 *
 * @description: A set of functions called "actions" for managing `Transaction`.
 */

const axios = require('axios')
const fs = require('fs')
// const request = require('request')

module.exports = {

  /**
   * Retrieve transaction records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.transaction.search(ctx.query);
    } else {
      return strapi.services.transaction.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a transaction record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.transaction.fetch(ctx.params);
  },


  transactionsForUser: async (ctx) => {
    return strapi.services.transaction.transactionsForUser(ctx.params);
  },
  /**
   * Count transaction records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.transaction.count(ctx.query);
  },

  /**
   * Create a/an transaction record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.transaction.add(ctx.request.body);
  },


  updateAll: async (ctx, next) => {
    return strapi.services.transaction.editAll(ctx.request.body);
  },



  /**
   * Update a/an transaction record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.transaction.edit(ctx.params, ctx.request.body);
  },



  /**
   * Destroy a/an transaction record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.transaction.remove(ctx.params);
  },

  auth_razorpay: async (ctx) => {
    let data = await strapi.services.transaction.auth_razorpay(ctx.request.body);

    return {}
  },
  getTransactionFromMobile: async (ctx) => {
    return await strapi.services.transaction.getTransactionFromMobile(ctx.request.body.mobile)
  },

  getHallTicket: async (ctx) => {
    return await strapi.services.transaction.getHallTicket(ctx.request.body.mobile)
  },

  downloadHallTicket: async (ctx) => {
    /*
      Doc:
        Not in use
        Used to download AdminCard @WP
    */
    ctx.response.attachment(`./public/NUET_AdmitCard.pdf`)
    ctx.status = 200
    // let url = `https://shaping3d.sgp1.digitaloceanspaces.com/upskill/${ctx.request.body._id}.pdf`
    // let res = request.get({ url, encoding: null })
    let res = fs.readFileSync(`./public/${ctx.request.body._id}.pdf`)
    ctx.send(res)
  },

  transaction_filter: async (ctx) => {
    return await strapi.services.transaction.transaction_filter(ctx.request.body)
  },

  transaction_filter_count: async (ctx) => {
    return await strapi.services.transaction.transaction_filter_count(ctx.request.body)
  },
  CustomUpdate: async (ctx, next) => {
    return strapi.services.transaction.CustomUpdate(ctx.params, ctx.request.body);
  },
  customCreateTransaction: async (ctx) => {
    return strapi.services.transaction.customCreateTransaction(ctx.request.body);
  }
};