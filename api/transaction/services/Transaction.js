'use strict';

/**
 * Transaction.js service
 *
 * @description: A set of functions similar to controller's actions to avoid code duplication.
 */

// Public dependencies.
const _ = require('lodash');
const fs = require('fs')
const axios = require('axios')
const html_pdf = require('html-pdf')
const sgMail = require('@sendgrid/mail');
const storage = require('node-persist')
sgMail.setApiKey('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')
// sgMail.setApiKey('SG.MlgbVboNSY-vhOEY6Mr0cg.Re0yWYz6EWhoxM42HQWH0IxsIUMycEyci0gqHMUSIaQ')
const AWS = require('aws-sdk')

const space = new AWS.S3({
  endpoint: new AWS.Endpoint('https://sgp1.digitaloceanspaces.com'),
  accessKeyId: 'ADCTWB6LPFNRHQEFW76W',
  secretAccessKey: 'la/iJ1S9zRK6U0WRiSMvHcI7hmzhSlpJa4p148rE9Qo'
})

module.exports = {

  /**
   * Promise to fetch all transactions.
   *
   * @return {Promise}
   */

  fetchAll: (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('transaction', params);
    // Select field to populate.
    const populate = Transaction.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Transaction
      .find()
      .where(filters.where)
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },

  /**
   * Promise to fetch a/an transaction.
   *
   * @return {Promise}
   */

  fetch: (params) => {
    // Select field to populate.
    const populate = Transaction.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Transaction
      .findOne(_.pick(params, _.keys(Transaction.schema.paths)))
      .populate(populate);
  },

  transactionsForUser: (params) => {
    /*
      Doc:
        Get transaction for participant using participants _id
        - In use @WP 
    */
    const populate = Transaction.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Transaction
      .findOne(_.pick(params, _.keys(Transaction.schema.paths)))
      .populate(populate);
  },

  /**
   * Promise to count transactions.
   *
   * @return {Promise}
   */

  count: (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('transaction', params);

    return Transaction
      .count()
      .where(filters.where);
  },

  /**
   * Promise to add a/an transaction.
   *
   * @return {Promise}
   */

  add: async (values) => {
    /*
    Doc:
      Create transaction if not found
      tz_id (uid) = add/update uid with TZ-**** id

      - In use @WP
    */

    // Extract values related to relational data.
    const relations = _.pick(values, Transaction.associations.map(ast => ast.alias));
    const data = _.omit(values, Transaction.associations.map(ast => ast.alias));

    let temp = await Transaction.findOne({ participants: relations.participants })

    if (temp == null) {

      // Tz-Id
      await storage.init();
      let tz_id = await storage.getItem('tz_id')
      if (tz_id == undefined) {
        tz_id = 2001
        await storage.setItem('tz_id', tz_id)
      } else {
        await storage.setItem('tz_id', tz_id + 1)
      }

      let uid = `TZ-${tz_id}`

      let participant = await Participants.update(
        { _id: relations.participants },
        { uid },
        { new: true }
      )


      // TODO: un-comment following block of code to activate remaining seat on transaction
      // also comment it from participant creation

      // try {
      //   // Remaining seats
      //   let remaining_seats = await storage.getItem('remaining_seats')
      //   if (remaining_seats == undefined) {
      //     await storage.setItem('remaining_seats', 1945)
      //   } else {
      //     await storage.setItem('remaining_seats', remaining_seats - 1)
      //   }
      // } catch (error) {
      //   console.log('remainig seats error', error)
      // }
      console.log("Transaction create -> add >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
      console.log(data, "data >>>>>>>>>>>")
      console.log(values, "values >>>>>>>>>>>>")
      console.log(relations, "Relations >>>>>>>>>>>>>")
      const entry = await Transaction.create(data);

      // Create relational data and return the entry.
      return Transaction.updateRelations({ _id: entry._id, values: relations });

    } else {
      console.log('transaction creation found')
      return { found: true }
    }
  },

  /**
   * Promise to edit a/an transaction.
   *
   * @return {Promise}
   */

  edit: async (params, values) => {
    /*
      Doc:
        Update transaction

        - In use @WP
    */
    // console.log(params, "params >>>>>>>>>>") // _id transcation
    // console.log(values, "values") //values data send to update
    if (values.checkTzId) {
      console.log(values);
      let _participant = await Participants.findOne({ _id: values.participants })

      let tz_id = _participant.uid;

      if (tz_id.charAt(0) + tz_id.charAt(1) == "TZ") {
        console.log("has tz-id already")
      }
      else {
        // console.log("in else")
        await storage.init();
        let tz_id = await storage.getItem('tz_id')
        if (tz_id == undefined) {
          tz_id = 2001
          await storage.setItem('tz_id', tz_id)
        } else {
          await storage.setItem('tz_id', tz_id + 1)
        }

        await Participants.update({ _id: values.participants }, { uid: `TZ-${tz_id}` });
      }
      delete values.participants;
    }


    // Extract values related to relational data.
    const relations = _.pick(values, Transaction.associations.map(a => a.alias));
    const data = _.omit(values, Transaction.associations.map(a => a.alias));


    // delete relations.participants;
    // Update entry with no-relational data.
    const entry = await Transaction.update(params, data, { multi: true });

    // console.log("Transaction update -> edit >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    // console.log(params, "Params >>>>>>>>>>>")
    // console.log(values, "values >>>>>>>>>>>>")
    // console.log(relations, "Relations >>>>>>>>>>>>>")

    // Update relational data and return the entry.
    return Transaction.updateRelations(Object.assign(params, { values: relations }));
  },

  CustomUpdate: async (params, values) => {

    console.log("------------- custom update query ------------")
    if (values.checkTzId) {
      let _participant = await Participants.findOne({ _id: values.participants })

      let tz_id = _participant.uid;

      if (tz_id.charAt(0) + tz_id.charAt(1) == "TZ") {
        console.log("has tz-id already")
      }
      else {
        // console.log("in else")
        await storage.init();
        let tz_id = await storage.getItem('tz_id')
        if (tz_id == undefined) {
          tz_id = 2001
          await storage.setItem('tz_id', tz_id)
        } else {
          await storage.setItem('tz_id', tz_id + 1)
        }

        await Participants.update({ _id: values.participants }, { uid: `TZ-${tz_id}` });
      }
    }
    // console.log(params._id, "params>>>>>>>>>>") // transcation _id to update
    // console.log(values, "values") // data passed to update
    return await Transaction.update({ _id: params._id }, values)
    // return true;
    // http://localhost:1337/transactionsCustomUpdate/5d0c6fb87d4e0c17eea1726d
    // {
    //   "status":"Confirmed",
    //   "participants":"5d120d8a4b70c5541670995c"
    // }
  },
  customCreateTransaction: async (values) => {
    console.log("Custom create transaction")
    const relations = _.pick(values, Transaction.associations.map(ast => ast.alias));
    const data = _.omit(values, Transaction.associations.map(ast => ast.alias));
    let temp = await Transaction.findOne({ participants: relations.participants })
    // console.log(relations.participants);

    if (temp == null) {

      // Tz-Id
      await storage.init();
      let tz_id = await storage.getItem('tz_id')
      if (tz_id == undefined) {
        tz_id = 2001
        await storage.setItem('tz_id', tz_id)
      } else {
        await storage.setItem('tz_id', tz_id + 1)
      }

      let uid = `TZ-${tz_id}`

      let participant = await Participants.update(
        { _id: relations.participants },
        { uid },
        { new: true }
      )
      data.participants = relations.participants;
      console.log(data, "before transaction creation")
      return await Transaction.create(data);
      console.log(entry1, "After creating transaction");
      return true;
    }
    else {
      console.log('transaction creation found')
      return { found: true }
    }
  },

  editAll: async (values) => {
    // Extract values related to relational data.
    const relations = _.pick(values, Transaction.associations.map(a => a.alias));
    const data = _.omit(values, Transaction.associations.map(a => a.alias));
    console.log("Transaction updateAll -> editAll >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    console.log(data, "data >>>>>>>>>>>")
    console.log(values, "values >>>>>>>>>>>>")
    console.log(relations, "Relations >>>>>>>>>>>>>")
    // Update entry with no-relational data.
    const entry = await Transaction.update({ _id: { $in: data.keys } }, { status: data.status }, { multi: true });

    // Update relational data and return the entry.
    return Transaction.updateRelations(Object.assign({ _id: { $in: data.keys } }, { values: relations }));
  },

  /**
   * Promise to remove a/an transaction.
   *
   * @return {Promise}
   */

  remove: async params => {
    // Select field to populate.
    const populate = Transaction.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    // Note: To get the full response of Mongo, use the `remove()` method
    // or add spent the parameter `{ passRawResult: true }` as second argument.
    const data = await Transaction
      .findOneAndRemove(params, {})
      .populate(populate);

    if (!data) {
      return data;
    }

    await Promise.all(
      Transaction.associations.map(async association => {
        if (!association.via || !data._id) {
          return true;
        }

        const search = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: data._id } : { [association.via]: { $in: [data._id] } };
        const update = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: null } : { $pull: { [association.via]: data._id } };

        // Retrieve model.
        const model = association.plugin ?
          strapi.plugins[association.plugin].models[association.model || association.collection] :
          strapi.models[association.model || association.collection];

        return model.update(search, update, { multi: true });
      })
    );

    return data;
  },

  /**
   * Promise to search a/an transaction.
   *
   * @return {Promise}
   */

  search: async (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('transaction', params);
    // Select field to populate.
    const populate = Transaction.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    const $or = Object.keys(Transaction.attributes).reduce((acc, curr) => {
      switch (Transaction.attributes[curr].type) {
        case 'integer':
        case 'float':
        case 'decimal':
          if (!_.isNaN(_.toNumber(params._q))) {
            return acc.concat({ [curr]: params._q });
          }

          return acc;
        case 'string':
        case 'text':
        case 'password':
          return acc.concat({ [curr]: { $regex: params._q, $options: 'i' } });
        case 'boolean':
          if (params._q === 'true' || params._q === 'false') {
            return acc.concat({ [curr]: params._q === 'true' });
          }

          return acc;
        default:
          return acc;
      }
    }, []);

    return Transaction
      .find({ $or })
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },

  getTransactionFromMobile: async (mobile) => {

    /*
      Doc:
        Get transaction and participant using mobile
 
        - In use @WP hall ticket page
    */

    let participant = await Participants.findOne({ mobile, is_active: true })
    if (participant == null) {
      return false
    } else {
      const populate = Transaction.associations
        .filter(ast => ast.autoPopulate !== false)
        .map(ast => ast.alias)
        .join(' ');
      let transaction = await Transaction.findOne({ participants: participant._id, status: 'Completed' })
        .populate(populate)

      // console.log(transaction)
      // console.log(participant)
      return transaction
    }
  },

  getHallTicket: async (mobile) => {

    /*
      Doc:
        Create Admin Card 
        upload it on DigitalOcean spaces 
        and send it on participants mail
 
        - In use @WP hall ticket page
    */

    // console.log('getHallTicket()')
    try {

      let participant = await Participants.aggregate([
        { $sort: { _id: -1 } },
        { $match: { mobile, is_active: true } },
        { $limit: 1 },
        {
          $lookup: {
            from: 'examvenue',
            localField: 'examvenue',
            foreignField: '_id',
            as: 'examvenue'
          }
        },
        { $unwind: '$examvenue' },
        {
          $lookup: {
            from: 'exam',
            localField: 'exam',
            foreignField: '_id',
            as: 'exam'
          }
        },
        { $unwind: '$exam' }
      ])

      participant = participant[0]

      var options = {
        format: 'A4',
        orientation: "portrait",
        header: {
          "height": "5mm",
          "contents": ' '
        },
      };
      // console.log("in get Hall ticket api")
      // console.log(participant)

      /* --------------------------------------------------------*/
      // Upload to DigitalOcean
      // ContentDisposition helps file rename at client side when downloading
      try {
        html_pdf.create(generateHallTicket(participant), options)
          .toBuffer(async function (err, fileBuffer) {
            // console.log(err)
            let _res = await space.putObject({
              Bucket: 'shaping3d/upskill',
              Key: `${participant._id}.pdf`,
              Body: fileBuffer,
              ACL: 'public-read',
              ContentDisposition: 'attachment; filename="NUET_Admit_Card.pdf"'
            }).promise()

            // console.log(_res, '_res')
          })
      } catch (error) {
        console.log(error)
      }

      /* --------------------------------------------------------*/

      /* Following block of code 
         create pdf file at server
         sends mail with pdf 
         and after tha mail has been sent 
         it removes it from server. */

      html_pdf.create(generateHallTicket(participant), options)
        .toFile(`./public/${participant._id}.pdf`, function (err, res) {
          pdfOverTheMail(res, participant)
        })
      return {
        filename: `${participant._id}.pdf`,
        error: false
      }

    } catch (err) {
      return {
        error: true,
        exception: err.toString()
      }
    }
  },

  auth_razorpay: async (body) => {

    /*
      Doc:
 
        Checks if transaction exits or not for certain participant
        if does not: create one and then create coupon code
        else: Update it
 
        Send sms and mails
 
        - In use as @RazorPayHook
    */
    // console.log(body,"Check >>>>>>>>>>>>>>>>.")
    let allow = false
    let email = body.payload.payment.entity.email
    let mobile = body.payload.payment.entity.contact
    let amount = parseFloat(body.payload.payment.entity.amount) / 100
    let code = body.payload.payment.entity.notes.code
    console.log(body)
    // return {}
    const populate = Participants.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    // participant
    mobile = mobile.substring(3);
    console.log(email, "Email >>>>>>>>>>>>")
    console.log(mobile, "mobile  >>>>>>>>>>>>")

    let participant = await Participants.findOne({
      $and: [

        {
          $or: [{ email }, { mobile }]
        },
        { is_active: true }
      ]
    }).populate(populate);



    // let participant_ = await Participants.aggregate(
    //   [
    //     { $sort: { "_id": -1 } },
    //     { $match: {email,mobile}},
    //     {
    //       $lookup:
    //       {
    //         from: "colleges",
    //         localField: "colleges",
    //         foreignField: "_id",
    //         as: "colleges",
    //       }
    //     }
    //     ,
    //     {
    //       $unwind: { "path": "$colleges" }
    //     },
    //     {
    //       $lookup:
    //       {
    //         from: "examvenue",
    //         localField: "examvenue",
    //         foreignField: "_id",
    //         as: "examvenue",
    //       }
    //     }
    //     ,
    //     {
    //       $unwind: { "path": "$examvenue" }
    //     },
    //     { $limit: 1 }

    //   ])

    // console.log(participant_, "Participant _")
    // // let participant = participant_[0];
    // console.log(participant, "participant")
    console.log(participant, "Participant check >>>>>>>>>>>")
    let examVenueParticipant = participant.examvenue.name;
    let splittedExamVenue = examVenueParticipant.split('-');
    let examVenueDate = splittedExamVenue[1];
    let examVenueVenue = splittedExamVenue[0];
    console.log(examVenueVenue, examVenueDate, "EXAM ///////////")
    setTimeout(async () => {
      try {


        let transaction = await Transaction.findOne({ participants: participant._id })
        // let transaction_ = await Transaction.aggregate(
        //   [
        //     { $sort: { "_id": -1 } },
        //     { $match: { participants: participant._id } },
        //     { $limit: 1 }

        //   ])
        // let transaction = transaction_[0];

        let couponcode_for_current_user = await Couponcode.findOne({ owner: participant._id })

        // let couponcode_for_current_user_ = await Couponcode.aggregate(
        //   [
        //     { $sort: { "_id": -1 } },
        //     { $match: { owner: participant._id } },
        //     { $limit: 1 }

        //   ])
        // let couponcode_for_current_user = couponcode_for_current_user_[0];
        // console.log(transaction, "Transcation>>>>>>>>>>>>>>.")
        // console.log(couponcode_for_current_user, "Coupon >>>>>>>>>>>>>>>>>>>>>>..")
        if (transaction == null) {
          console.log('transaction creation on null from hook')
          allow = true
          transaction = Transaction.create({
            paid_via: {
              "initial": "online",
              "rem": ""
            },
            Amount: {
              "initial": amount.toString(),
              "rem": "0"
            },
            collected_by: {
              "initial": `${body.payload.payment.entity.id}`,
              "rem": ""
            },
            status: 'Completed',
            participants: participant._id,
            applied_code: code
          })


          // Tz-Id
          await storage.init();
          let tz_id = await storage.getItem('tz_id')
          if (tz_id == undefined) {
            tz_id = 2001
            await storage.setItem('tz_id', tz_id)
          } else {
            await storage.setItem('tz_id', tz_id + 1)
          }

          let update_participant = await Participants.update({ _id: participant._id }, { uid: `TZ-${tz_id}` }, { multi: true });

          // console.log(update_participant, "Update participant>>>>")
          participant.uid = `TZ-${tz_id}`

          console.log(participant, "After tz id creation >>>>>>>>")
          // TODO: un-comment following block of code to activate remaning seat on transaction
          // also comment it from participant creation

          // try {
          //   // Remaining seats
          //   let remaining_seats = await storage.getItem('remaining_seats')
          //   if (remaining_seats == undefined) {
          //     await storage.setItem('remaining_seats', 1945)
          //   } else {
          //     await storage.setItem('remaining_seats', remaining_seats - 1)
          //   }
          // } catch (error) {
          //   console.log('remainig seats error', error)
          // }

        } else {
          // console.log(3)
          if (transaction.status != 'Completed') {
            console.log('transaction update from hook')
            allow = true
            transaction = await Transaction.update({ _id: transaction._id },
              {
                paid_via: {
                  ...transaction.paid_via,
                  "rem": "online"
                },
                Amount: {
                  ...transaction.Amount,
                  "rem": amount.toString()
                },
                collected_by: {
                  ...transaction.collected_by,
                  "rem": `${body.payload.payment.entity.id}`,

                },
                status: 'Completed',
                applied_code: code
              })
          }

        }


        if (allow) {
          // console.log(5)

          const populate2 = Couponcode.associations
            .filter(ast => ast.autoPopulate !== false)
            .map(ast => ast.alias)
            .join(' ');

          if (couponcode_for_current_user == null) {
            couponcode_for_current_user = await Couponcode.create({
              unique_code: makeid(),
              owner: participant._id
            })
          }

          let couponcode_of_referred_user = null
          if (code != '') {
            couponcode_of_referred_user = await Couponcode
              .findOne({ unique_code: code })
              .populate(populate2)
          }

          let data = {
            participant,
            transaction,
            couponcode_for_current_user,
            couponcode_of_referred_user,
            code,
            email,
            amount,
            mobile
          }

          // console.log(6)
          if (data.participant != null) {
            // console.log(7)

            let name = titleCase(getFirstName(data.participant.name))

            // console.log(name, "Name before mailing >>>")

            try {
              // internal mail for payment
              let sub = ''
              if (data.code != '') {
                sub += '(Referred) '
              }
              sub += `payment ${data.participant.name} | ${data.participant.uid} | ${data.participant.colleges.name}`

              await strapi.plugins['email'].services.email.send({
                to: "upskill@techzillaindia.com",
                from: "upskill@techzillaindia.com",
                subject: sub,
                html: generatePaymentMsg(data, body.payload.payment.entity.id, data.amount, data.code, data.couponcode_of_referred_user)
              });
            } catch (error) {
              console.log('email', error)
            }


            try {
              // seat confirmed / enrolled
              await strapi.plugins['email'].services.email.send({
                to: body.payload.payment.entity.email,
                from: "upskill@techzillaindia.com",
                subject: 'Successfully Enrolled',
                html: successfullyEnrolledTemplate(data.participant.uid, name, examVenueVenue, examVenueDate)
              });

            } catch (error) {
              console.log('email', error)
            }

            try {
              // referrel code for current user
              await strapi.plugins['email'].services.email.send({
                to: body.payload.payment.entity.email,
                from: "upskill@techzillaindia.com",
                subject: 'Get Rs.100 cash reward on referring your friends!',
                html: generateOwnerReferCodeTemplate(data.couponcode_for_current_user, name, examVenueVenue, examVenueDate)
              });

            } catch (error) {
              console.log('email', error)
            }


            try {
              // mail for the user whos refer code used
              if (data.code != '' && couponcode_of_referred_user != null) {
                await strapi.plugins['email'].services.email.send({
                  to: data.couponcode_of_referred_user.owner.email,
                  from: "upskill@techzillaindia.com",
                  subject: 'Congratulations! You have a new referral.',
                  html: generateReferredTemplate(data.couponcode_of_referred_user, name)
                });

                sendSms(
                  data.couponcode_of_referred_user.owner.mobile,
                  `Congratulations! your friend ${name} has enrolled with us. Please check mail for more details.`)
              }

            } catch (error) {
              console.log('email', error)
            }

            // console.log(8)
            // default sms after enrollment
            sendSms(data.mobile, `We have successfully received your payment for NUET. All the important updates will be sent to your number and email. Download Syllabus @ http://bit.ly/nuetsyb. Cheers and all the best!`)

            // console.log(9)
            // seat confirmed
            sendSms(data.mobile,
              `Hey ${name},\nYour seat for the National upskill entrance Test is confirmed with seat no ${data.participant.uid}. For more details on National Upskill programme visit http://bit.ly/TZUPSKILL`)

            // console.log(10)
            // refer ur friend
            sendSms(data.mobile,
              `Refer your friends using code: ${data.couponcode_for_current_user.unique_code} and get Rs.100 cash reward on each successful enrollement. Please check mail for more details.`)
          }

        }

      } catch (error) {
        console.log('web hook error', error)
      }
    }, 5000)
  },


  transaction_filter: async (body) => {

    /*
      Doc:
        Not in use
    */

    let query = buildQuery(body)

    query.push({ $skip: parseInt(body.currentIndex) })
    query.push({ $limit: parseInt(body.numberOfRows) })

    return await Transaction.aggregate([
      ...query
    ])
  },

  transaction_filter_count: async (body) => {
    /*
      Doc:
        Not in use
    */

    let query = buildQuery(body)

    let transaction = await Transaction.aggregate([
      ...query,
      {
        $count: "count"
      }
    ])


    let count = 0
    if (transaction.length > 0) {
      count = transaction[0].count
    }
    return { count }
  }
};


const buildQuery = (body) => {

  // Doc: Not in use

  /* 
    Collection names:
    colleges
    couponcode
    participants
    transactions
  */

  let query = [
    { $sort: { '_id': -1 } },
    {
      $lookup: {
        from: 'participants',
        localField: 'participants',
        foreignField: '_id',
        as: 'participant'
      }
    },
    { $unwind: '$participant' },
    {
      $lookup: {
        from: 'colleges',
        localField: 'participant.colleges',
        foreignField: '_id',
        as: 'college'
      }
    },
    { $unwind: '$college' },
    {
      $lookup: {
        from: 'couponcode',
        localField: 'participant._id',
        foreignField: 'owner',
        as: 'couponCode'
      }
    },
    // { $unwind: '$couponCode' },
  ]


  let matchQuery = { $match: { $or: [] } }

  if (body.search_text != undefined && body.search_text != '') {
    let txt = body.search_text.trim()
    matchQuery.$match.$or.push({ 'participant.name': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'participant.email': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'participant.mobile': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'participant.city': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'participant.state': { $regex: txt, $options: 'i' } })
    // matchQuery.$match.$or.push({ 'participant.dob': { $regex: txt, $options: 'i' } }) // need extra efforts for date
    matchQuery.$match.$or.push({ 'participant.uid': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'college.name': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'couponCode.unique_code': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'participant.applied_code': { $regex: txt, $options: 'i' } })

    query.push(matchQuery)
  }

  if (body.status != undefined && body.status != '') {
    query.push({
      $match: { "status": { $regex: body.status, $options: 'i' } }
    })
  }

  if (body.time_slot != undefined && body.time_slot != '') {
    query.push({
      $match: { "participant.time_slots": { $regex: body.time_slot, $options: 'i' } }
    })
  }

  return query
}


const makeid = () => {
  // console.log('markid')
  var text = "TZ";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}


// Doc:
const sendSms = (nbs, txt) => {
  // 256197A9qclHWiyea5c386431 --> new key
  // 256184AFYipcdt35c385165 --> old key
  const uri = encodeURI(`http://api.msg91.com/api/sendhttp.php?country=91&sender=TZILLA&route=4&mobiles=${nbs}&authkey=256197A9qclHWiyea5c386431&message=${txt}`)

  axios.get(uri).then(res => {
    // console.log(res)
  }).catch(err => {
    // console.log(err)
  })
}

// Doc: Hall ticket template
function generateHallTicket(user) {
  // console.log("gen hallticket function")
  // console.log(user)
  let venue_date = user.exam ? user.exam.name.split('-') : ["", ""]
  return `<!DOCTYPE html>
  <html lang="en">
  
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Document</title>
      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  
      <script src="https://code.jquery.com/jquery-3.3.1.min.js"
          integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  
  
      <style>
      #tbody>tr {
        font-size: 18px
    }

    body {
        margin: 25px;
    }

    table th,
    td {
        text-align: center;
        padding: 10px;
        
    }

    table td {
        font-size: 11px;
    }

    table th {
        background: url("http://142.93.213.87:1337/uploads/66d3510bd2ca4d51a90896585a0c1bf1.jpg") !important;
        webkit-print-color-adjust: exact;
        font-size: 11px;
    }

    .title {
        font-size: 18px;
        font-weight: 500;
        margin-bottom: 7px;
    }

    .sub-title {
        font-size: 11px;
        font-weight: 500;
        margin-left: 100px;
        letter-spacing: 1px;
    }

    ol li {
        font-size: 11px;
    }

          .borderT{
            border-top:1px solid black;
          
          }

          .borderR{
            border-right:1px solid black;
          }

          .borderB{
            border-bottom:1px solid black;
          }

          .borderL{
            border-Left:1px solid black;
          }
      </style>
  
  </head>
  
  <body>
  
      <!-- hall ticket -->
  
      <div class="container" id="hallticketDiv">
  
          <div style="width:100%;margin:auto;border:1px solid black;">
              <!-- 1st box -->
              <div style="width: 100%;margin: auto;">
                  <div class="row">
                      <div style="text-align:center;margin-top: 12px;" class="col-xs-4">
                          <img style="padding: 5px 5px 5px 8px;"
                              src="https://techzillaindia.com/upskill/wp-content/uploads/2019/01/BLACK.png" alt=""
                              height="50" width="160">
                      </div>
                      <div class="col-xs-8" style="text-align: left; margin-top:3px;">
                          <h2 class="title">NATIONAL UPSKILL ENTRANCE TEST</h2>
                          <p class="sub-title">HALL TICKET ( 2019 )</p>
                      </div>
                  </div>
              </div>
  
  
              <!-- 2nd box -->
              <div style="margin-top:13px; margin-bottom:5px;">
                  <div class="row">
                      <div class="col-xs-12">
  
                          <table style="width:512px;">
                              <tbody id="tbody">
                                  <tr class="tempRow">
                                      <th class="borderT borderR borderL">Seat No.</td>
                                      <th class="borderT borderR">DOB</td>
                                      <th class="borderT borderR">Origin</td>
                                  </tr>
                                  <tr>
                                      <td class="borderT borderR borderL">${user.uid}</td>
                                      <td class="borderT borderR">${user.dob}</td>
                                      <td class="borderT borderR">${user.city}, ${user.state}</td>
                                  </tr>
                                  <tr>
                                      <td  class="borderT borderR borderL" style='font-weight: bold; font-size:11px;'>Candidate' s Name</td>
                                      <td class="borderT borderR" colspan="3" style='text-align:center;text-transform: uppercase;'><b>${user.name}</td>
  
                                  </tr>
                                  <tr class="tempRow">
  
                                      <th class="borderT borderR borderL">Examination Time</td>
                                      <th class="borderT borderR ">Examination Date</td>
                                      <th class="borderT borderR">Examination Venue</td>
                                  </tr>
                                  <tr>
  
                                      <td class="borderT borderR borderB borderL">${user.time_slots}</td>
                                     
                                      <td class="borderT borderB borderR">${venue_date[0].trim()}</td>
                                      <td class="borderT borderR borderB" style="width:45%;">${venue_date[1].trim()}</td>
                                  </tr>
                              </tbody>
                          </table>
                      </div>
                  </div>
  
                  <div class='row' style='position: relative'>
                      <div class='col-xs-8' style="margin-top:10px;">

                              <p style='font-weight:500; font-size:14px; margin-left:5px;'>NOTE:</b>
                                  <ol style="padding-left: 22px">
                                      <li>
                                          Candidates shall not be allowed without a <b>valid government ID proof</b>.
                                      </li>
                                      <li>
                                          <b>No extra time</b> will be given in any case.
                                      </li>
                                      <li>
                                          Reporting time is <b>strictly one hour before</b> the examination time. <b style="color:#f00 !important;">No late entries shall be entertained at any cost.</b>
                                      </li>
                                      <li>
                                          Mobile phone or any other electronic device is strictly prohibited in the
                                          examination hall.
                                      </li>
                                      <li>
                                          No candidate shall be allowed without hallticket.
                                      </li>
                                      
                                  </ol>
  
                      </div>
                      <div class='col-xs-4' style='text-align:center; margin-top:14px;'>
                          <img id="userImage" src="${user.pic}" alt="user image" height="110" width="100" />
                          <div style="height: 45px;">
  
                          </div>
                          <p style='font-weight:bold; text-transform: uppercase; font-size: 10px;'>
                              <!-- position: absolute; right:193px; bottom:2px -->
                              Candidate's signature
                          </p>
                      </div>
  
                  </div>
  
  
              </div>
  
  
          </div>
      </div>
  </body>
  
  </html>`
}

const generateReferredTemplate = (referredUser, name) => {
  // console.log('generateReferredTemplate')
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Neopolitan Confirm Email</title>
<!-- Designed by https://github.com/kaytcat -->
<!-- Robot header image designed by Freepik.com -->

<style type="text/css">
@import url(http://fonts.googleapis.com/css?family=Droid+Sans);

/* Take care of image borders and formatting */

p {
font-size: 18px !important
}

img {
max-width: 600px;
outline: none;
text-decoration: none;
-ms-interpolation-mode: bicubic;
}

a {
text-decoration: none;
border: 0;
outline: none;
color: #bbbbbb;
}

a img {
border: none;
}

/* General styling */

td,
h1,
h2,
h3 {
font-family: Helvetica, Arial, sans-serif;
font-weight: 400;
}

td {
text-align: center;
}

body {
-webkit-font-smoothing: antialiased;
-webkit-text-size-adjust: none;
width: 100%;
height: 100%;
color: #37302d;
background: #ffffff;
font-size: 16px;
}

table {
border-collapse: collapse !important;
}

.headline {
color: #ffffff;
font-size: 36px;
}

.force-full-width {
width: 100% !important;
}

.force-width-80 {
width: 80% !important;
}
</style>

<style type="text/css" media="screen">
@media screen {

/*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
td,
h1,
h2,
h3 {
font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
}
}
</style>

<style type="text/css" media="only screen and (max-width: 480px)">
/* Mobile styles */
@media only screen and (max-width: 480px) {

table[class="w320"] {
width: 320px !important;
}

td[class="mobile-block"] {
width: 100% !important;
display: block !important;
}


}
</style>
</head>

<body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
bgcolor="#ffffff">
<table align="center" cellpadding="0" cellspacing="0" class="force-full-width" height="100%">
<tr>
<td align="center" valign="top" bgcolor="#ffffff" width="100%">
<center>
<table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="600" class="w320">
  <tr>
    <td align="center" valign="top">

      <table style="margin: 0 auto;background: #010e28" cellpadding="0" cellspacing="0" class="force-full-width"
        style="margin:0 auto;">
        <tr>
          <td style="font-size: 15px; text-align:center;">
            <br>
            <img src="https://techzillaindia.com/upskill/wp-content/uploads/2019/01/WHITE.png" height="50"
              width="160" alt="">
            <br>
            <br>
          </td>
        </tr>
      </table>

      <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#4dbfbf">
        <tr>
          <td>
            <br>
            <img src="https://techzillaindia.com/template_images/robowave_03.gif" width="224" height="240" alt="robot picture">
          </td>
        </tr>
        <tr>
          <td class="headline">
            Congratulations!
          </td>
        </tr>
        <tr>
          <td>

            <center>
              <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="60%">
                <tr>
                  <td style="color:#083d3d;text-align: left;">
                    <br>

                    <p>Hey <span style="color:black"><b>${titleCase(getFirstName(referredUser.owner.name))}</b></span>,</p>

                    Your friend <b>${name}</b> has sucessfully enrolled with us for NUET.

                    <p>Once you refer 3 friends, you can request a cashout via WhatsApp message on this 7304322623.</p>

                    <br/>
                    We shall verify & issue your cashout in 2 working days. 
                    <br/>
                    Payment method- Bank Transfer only.
                    <br/>
                    Details needed for cashout
                    <br/>
                    Payee Name
                    <br/>
                    Account Number
                    <br/>
                    IFSC -CODE
                    <br/>
                    Branch Name
                    <br/><br/>               
                    <p>Keep sharing National Upskill programme by referring more friends & earn cash rewards.</p>
                    <br>
                    <br>
                  </td>
                </tr>
              </table>
            </center>

          </td>
        </tr>
      </table>


      <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#414141"
        style="margin: 0 auto">
        <tr>
          <td style="color:#bbbbbb; font-size:15px">
            <br>
            <span style="text-align: center;">
                                        For any further assistance email us at <b style="color:black">upskill@techzillaindia.com</b>.
                                        </span>

          </td>
        </tr>

        <tr>

          <td style="color:#bbbbbb; font-size:12px;">
            <br>
            <br>
            <span>National UpSkill program by Techzilla India Infotech</span>

          </td>
        </tr>
        <tr>
          <td style="color:#bbbbbb; font-size:12px;">
            <br>
            © 2019 All Rights Reserved
            <br>
            <br>
            <br>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</center>
</td>
</tr>
</table>
</body>

</html>`
}


const generateOwnerReferCodeTemplate = (data, name, examVenue, examDate) => {
  // console.log('generateOwnerReferCodeTemplate')
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Neopolitan Confirm Email</title>
<!-- Designed by https://github.com/kaytcat -->
<!-- Robot header image designed by Freepik.com -->

<style type="text/css">
@import url(http://fonts.googleapis.com/css?family=Droid+Sans);

/* Take care of image borders and formatting */

p {
font-size: 18px !important
}

img {
max-width: 600px;
outline: none;
text-decoration: none;
-ms-interpolation-mode: bicubic;
}

a {
text-decoration: none;
border: 0;
outline: none;
color: #bbbbbb;
}

a img {
border: none;
}

/* General styling */

td,
h1,
h2,
h3 {
font-family: Helvetica, Arial, sans-serif;
font-weight: 400;
}

td {
text-align: center;
}

body {
-webkit-font-smoothing: antialiased;
-webkit-text-size-adjust: none;
width: 100%;
height: 100%;
color: #37302d;
background: #ffffff;
font-size: 16px;
}

table {
border-collapse: collapse !important;
}

.headline {
color: #ffffff;
font-size: 36px;
}

.force-full-width {
width: 100% !important;
}

.force-width-80 {
width: 80% !important;
}
</style>

<style type="text/css" media="screen">
@media screen {

/*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
td,
h1,
h2,
h3 {
font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
}
}
</style>

<style type="text/css" media="only screen and (max-width: 480px)">
/* Mobile styles */
@media only screen and (max-width: 480px) {

table[class="w320"] {
width: 320px !important;
}

td[class="mobile-block"] {
width: 100% !important;
display: block !important;
}


}
</style>
</head>

<body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
bgcolor="#ffffff">
<table align="center" cellpadding="0" cellspacing="0" class="force-full-width" height="100%">
<tr>
<td align="center" valign="top" bgcolor="#ffffff" width="100%">
<center>
<table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="600" class="w320">
  <tr>
    <td align="center" valign="top">

      <table style="margin: 0 auto;background: #010e28" cellpadding="0" cellspacing="0" class="force-full-width"
        style="margin:0 auto;">
        <tr>
          <td style="font-size: 15px; text-align:center;">
            <br>
            <img src="https://techzillaindia.com/upskill/wp-content/uploads/2019/01/WHITE.png" height="50"
              width="160" alt="">
            <br>
            <br>
          </td>
        </tr>
      </table>

      <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#4dbfbf">
        <tr>
          <td>
            <br>
            <img src="https://techzillaindia.com/template_images/robowave_03.gif" width="224" height="240" alt="robot picture">
          </td>
        </tr>
        <tr>
          <td class="headline">
            Congratulations!
          </td>
        </tr>
        <tr>
          <td>

            <center>
              <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="60%">
                <tr>
                  <td style="color:#083d3d;text-align: left;">
                    <br>

                    <p>Hey <span style="color:black"><b>${name}</b></span>,</p>

                 <p>   Greetings from Techzilla India!
                    You can earn Rs. 100 cash reward on each successful enrollment of your friend and your friend gets 100 instant discount too.
                    <br/>
                    <br/>
                    Share the Referral code: <b>${data.unique_code}</b>,  
                    with your friends and ask them to register for NUET: <a href="https://techzillaindia.com/upskill/registration">https://techzillaindia.com/upskill/registration</a>
                    <br/>
                    <br/>
                    On every successful registration of your friend you will get an email & sms of his enrollment. 
                    <br/>
                    <br/>
                    Once you refer 3 friends, you can request a cashout via WhatsApp message on this 7304322623. 
                    <br/>
                    <br/>
                    We shall verify & issue your cashout in 2 working days. 
                    <br/>
                    <br/>
                    Minimum 3 referral required for cashout.
                    <br/>
                    <br/>
                    Payment method- Bank Transfer only.
                    <br/>
                    Details needed for cashout
                    <br/>
                    Payee Name
                    <br/>
                    Account Number
                    <br/>
                    IFSC CODE
                    <br/>
                    Branch Name
                    </p>
                    
                    <br>

                    <h3>You can use this message to forward in your groups.</h3>
                    
                    <p>
                    *Dream job* with a guaranteed package of around *7LPA* but don't have enough skills ?
                      We have got your back 📇
                      <br>

                      *Techzilla India* & *E Cell, IIT Bombay* present <br>
                      *National Upskill Entrance Test* <br>
                      A search for India's next gen technology leaders <br>
                      <br>
                      What is *National Upskill Programme?*
                      <br>
                      💻100 Days no cost Intensive training programme.
                      <br>
                      💻Assured placement of INR 7LPA Avg salary. 
                      <br>
                      💻 Hands on experience on technologies used by Big giants like Facebook, Microsoft,Flipkart,Airbnb etc.
                      <br>
                      💻Training by industry experts & Live projects and certification at no cost.
                      <br>
                      *No CGPA Criteria* : Students studying in Final year or graduates from science stream(BTech,BE,ME,MCA,BCA,Bsc,MSc,diploma )
                      <br>
                      <br>
                      *Our process*
                      1.NUET(Entrance Test) <br>
                      2.Screening<br>
                      3.Training<br>
                      4.Get Placed. <br>
                      <br>
                      For more details on the training module & about the programme visit https://techzillaindia.com/upskill
                      <br>
                
                      Limited seats and first come first serve basis <br>

                      Register now at https://techzillaindia.com/upskill/registration <br>
                      Use my code ${data.unique_code} to get INR 100/- instant discount on the application fee.<br>
                      
                      </p>
                  </td>
                </tr>
              </table>
            </center>

          </td>
        </tr>
      </table>


      <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#414141"
        style="margin: 0 auto">
        <tr>
          <td style="color:#bbbbbb; font-size:15px">
            <br>
            <span style="text-align: center;">
              For any further assistance email us at <b style="color:black">upskill@techzillaindia.com</b>.
            </span>

          </td>
        </tr>

        <tr>

          <td style="color:#bbbbbb; font-size:12px;">
            <br>
            <br>
            <span>National UpSkill program by Techzilla India Infotech</span>

          </td>
        </tr>
        <tr>
          <td style="color:#bbbbbb; font-size:12px;">
            <br>
            © 2019 All Rights Reserved
            <br>
            <br>
            <br>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</center>
</td>
</tr>
</table>
</body>

</html>`
}


const successfullyEnrolledTemplate = (uid, full_name, examVenue, examDate) => {
  // console.log('successfullyEnrolledTemplate')
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Neopolitan Confirm Email</title>
<!-- Designed by https://github.com/kaytcat -->
<!-- Robot header image designed by Freepik.com -->

<style type="text/css">
@import url(http://fonts.googleapis.com/css?family=Droid+Sans);

/* Take care of image borders and formatting */

p{
  font-size:15px !important
}

img {
max-width: 600px;
outline: none;
text-decoration: none;
-ms-interpolation-mode: bicubic;
}

a {
text-decoration: none;
border: 0;
outline: none;
color: #bbbbbb;
}

a img {
border: none;
}

/* General styling */

td,
h1,
h2,
h3 {
font-family: Helvetica, Arial, sans-serif;
font-weight: 400;
}

td {
text-align: center;
}

body {
-webkit-font-smoothing: antialiased;
-webkit-text-size-adjust: none;
width: 100%;
height: 100%;
color: #37302d;
background: #ffffff;
font-size: 16px;
}

table {
border-collapse: collapse !important;
}

.headline {
color: #ffffff;
font-size: 36px;
}

.force-full-width {
width: 100% !important;
}

.force-width-80 {
width: 80% !important;
}
</style>

<style type="text/css" media="screen">
@media screen {

/*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
td,
h1,
h2,
h3 {
font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
}
}
</style>

<style type="text/css" media="only screen and (max-width: 480px)">
/* Mobile styles */
@media only screen and (max-width: 480px) {

table[class="w320"] {
width: 320px !important;
}

td[class="mobile-block"] {
width: 100% !important;
display: block !important;
}


}
</style>
</head>

<body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
bgcolor="#ffffff">
<table align="center" cellpadding="0" cellspacing="0" class="force-full-width" height="100%">
<tr>
<td align="center" valign="top" bgcolor="#ffffff" width="100%">
<center>
<table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="600" class="w320">
  <tr>
    <td align="center" valign="top">

      <table style="margin: 0 auto;background: #010e28" cellpadding="0" cellspacing="0" class="force-full-width"
        style="margin:0 auto;">
        <tr>
          <td style="font-size: 15px; text-align:center;">
            <br>
            <img src="https://techzillaindia.com/template_images/upskill.png" height="50"
              width="160" alt="">
            <br>
            <br>
          </td>
        </tr>
      </table>

      <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#4dbfbf">
        <tr>
          <td>
            <br>
            <img src="https://techzillaindia.com/template_images/robomail.gif" width="224" height="240" alt="robot picture">
          </td>
        </tr>
        <tr>
          <td class="headline">
            Successfully Enrolled
          </td>
        </tr>
        <tr>
          <td>

          <center>
          <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="70%">
            <tr>
              <td style="color:#083d3d;text-align: left;padding:5px;">
                <br>

                <p>Hey <span style="color:black"><b>${full_name}</b></span>,</p>

                <p>Congratulations !!!</p>
               
                Your seat for the National Upskill Entrance Test (NUET) is confirmed with 
                seat no <span style="color:black"><b>${uid}</b></span>,
                which will be held on <span style="color:black"><b>${examDate}</b></span> 
                at <span style="color:black"><b>${examVenue}</b></span>.
                
                <p>Gear up for NUET !</p>
                <p>For any further information
                  <a href="https://www.techzillaindia.com/upskill" style="color: black;text-decoration: underline">Click
                    here</a></p>
               
                
                <p>
                  You can generate the Admit card from 
                  <a style="color: black;text-decoration: underline" href="https://techzillaindia.com/upskill/hallticket/">
                    here
                  </a>
                    Download detailed Syllabus  
                    <a style="color:#53f6c6;" href="http://bit.ly/nuetsyb">
                    here
                    </a>
                  </p>
              </td>
            </tr>
          </table>
        </center>

          </td>
        </tr>
      </table>


      <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#414141"
        style="margin: 0 auto">
        <tr>
          <td style="color:#bbbbbb; font-size:15px">
            <br>
            <span style="text-align: center;">
              For any further assistance email us at <b style="color:black">upskill@techzillaindia.com</b>.
            </span>
          </td>
        </tr>

        <tr>

          <td style="color:#bbbbbb; font-size:12px;">
              <br/>
            <span>National UpSkill program by Techzilla India Infotech</span>

          </td>
        </tr>
        <tr>
          <td style="color:#bbbbbb; font-size:12px;">
              <br/>
            © 2019 All Rights Reserved
            <br>
            <br>
            <br>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</center>
</td>
</tr>
</table>
</body>
</html>`
}


const generatePaymentMsg = (data, razor_pay_id, amount, code, referredUser) => {
  // console.log('generatePaymentMsg')
  let code_owner = code != '' && referredUser != null
    ? `<p>Referrered By: <b>${referredUser.owner.name}</b></p>`
    : '<p></p>'
  try {
    return `<!DOCTYPE html>
                    <html lang="en">

                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title></title>
                    </head>

                    <body>
                        ${code_owner}
                        <p>Razor Pay: <b>${razor_pay_id}</b>, Amount: <b>${amount}</b></p>
                        <p>Name: <b>${data.participant.name}</b></p>
                        <p>TzId: <b>${data.participant.uid}</b></p>
                        <p>DOB: <b>${data.participant.dob}</b></p>
                        <p>Gender: <b>${data.participant.gender}</b></p>
                        <p>Mobile: <b>${data.participant.mobile}</b></p>
                        <p>Email: <b>${data.participant.email}</b></p>
                        <p>College: <b>${data.participant.colleges.name}</b></p>
                        <p>State: <b>${data.participant.state}</b></p>
                        <p>City: <b>${data.participant.city}</b></p>
                        <p>Picode: <b>${data.participant.pincode}</b></p>
                        <p>Created at: <b>${data.participant.createdAt}</b></p>
                        

                    </body>

                    </html>`
  } catch (error) {
    return `Something went wrong 
            <br/> 
            ${JSON.stringify(data)} | ${razor_pay_id} | ${amount}`
  }

}


// Doc: send pdf over the main and remove it
function pdfOverTheMail(res, participant) {

  // console.log('called.. pdfOverTheMail()', res)
  try {
    fs.readFile(res.filename, { encoding: 'base64' }, (err, data) => {
      // console.log(err, data, '============= file read ===========')
      const msg = {
        to: participant.email,
        from: 'upskill@techzillaindia.com',
        subject: 'Important instructions for NUET & Hall ticket',
        attachments: [{
          filename: 'NUET_AdmitCard.pdf',
          content: data,
          type: 'application/pdf',
          disposition: 'attachment',
          contentId: participant._id
        }],
        html: `<p>Hi ${titleCase(getFirstName(participant.name))}<br/><br/>.<b>Things to Bring</b>:- ( Entry to the exam can be declined in case of below-mentioned documents are missing )<br/>- Admit Card / Hall Ticket<br/>-Government ID Proof<br/><br/><b>Exam Rules</b>:<br/>-No mobile phones inside the classrooms allowed<br/>-Only Simple calculators allowed [No scientific Calculators]<br/>-Cheating in any sense will result in disqualification & right to admission is reserved to the management<br/>-Exam Duration - 90 Minutes<br/>-Black / Blue Ball Pens only.<br/><br/><b>Please find below Google map location for the venues<b>:<br/><br/>1.11th August, Sunday, <a href="https://goo.gl/maps/xW4jSzVvX9p8HrmH6">Lecture hall complex, IIT Bombay</a><br/><br/>2.18th August, Sunday, <a href="https://maps.app.goo.gl/xZD1bZE8XqSvrfTs9">SPS 7, Delhi Technological University</a><br/><br/>3.25th August, Sunday, <a href="https://goo.gl/maps/vh51tCDdgM7zrVWd7">Academic Complex, COEP</a><br/><br/><b>NOTE: Date, time & venue is as per your selection which cannot be changed. Please refer to the admit card attached with the mail</b><br/><br/> All the best for NUET!</p>`
      }
      sgMail.send(msg, (err, response) => {
        console.log('==> pdf sent', err)
        fs.unlink(res.filename, function () { })
      })
    })
  } catch (error) {
    console.log('pdfOverTheMail() error', error)
  }


  // console.log('End')
}

const titleCase = (str) => {
  var splitStr = str.toLowerCase().split(' ');
  for (var i = 0; i < splitStr.length; i++) {
    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  return splitStr.join(' ');
}

const getFirstName = (str) => {

  var name = str.split(" ");
  return name[0]
}
