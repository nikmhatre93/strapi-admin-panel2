'use strict';

/**
 * Participants.js service
 *
 * @description: A set of functions similar to controller's actions to avoid code duplication.
 */

// Public dependencies.
const _ = require('lodash');
// const http = require('http')
var axios = require('axios')
const storage = require('node-persist')
var mongoose = require('mongoose');
const moment = require('moment')
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey('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') // Live Key


// Agenda lib (https://github.com/agenda/agenda)
const Agenda = require('agenda')
const task = new Agenda({ db: { address: 'mongodb://127.0.0.1/admindashboard2' } })

// Task
task.define('send_mail', (job, done) => {
  axios.get('https://upskilladmin.techzillaindia.com/send_schedule_mails')
    .then(res => {
      console.log('res')
      done()
    })
    .catch(err => {
      done()
    })
})

// scheduler
const scheduler = async () => {
  await task.start();
  await task.every('1 hours', 'send_mail');
};

scheduler()

module.exports = {

  send_schedule_mails: async () => {
    // Email scheduler api to send mails
    let lt = moment().subtract(1, 'hours').format('lll')
    let gt = moment().subtract(2, 'hours').format('lll')

    // console.log(lt, gt)

    let participants = await Participants.aggregate([
      {
        $match: {
          is_mail_sent: false,
          is_active: true,
          createdAt: {
            $lt: new Date(lt),
            $gte: new Date(gt),
          }
        }
      },
      {
        $lookup: {
          from: 'transactions',
          localField: '_id',
          foreignField: 'participants',
          as: 'transaction'
        }
      },
      { $match: { transaction: { $eq: [] } } }

    ])

    // console.log(participants)
    if (participants) {
      participants.forEach(participant => {
        console.log(participant.name, 'mail sent to')
        const msg = {
          to: participant.email,
          from: 'upskill@techzillaindia.com',
          subject: 'Your NUET application is incomplete',
          html: getTemplate(titleCase(getFirstName(participant.name))),
        };

        sgMail.send(msg, (err, res) => {
          // console.log(res, err)
          if (err) { }
          else {
            Participants.update({ _id: participant._id }, { is_mail_sent: true })
              .exec((err1, res1) => {
                console.log(res1, err1)
              })
          }
        })

      })
    }

    return true
  },

  /**
   * Promise to fetch all participants.
   *
   * @return {Promise}
   */

  fetchAll: (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('participants', params);
    // Select field to populate.
    const populate = Participants.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    // console.log(Transaction.find())

    return Participants
      .find()
      .where(filters.where)
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },

  findAllWithTransactionRelation: async (params) => {
    return await Participants.find({ transactions: { $ne: null } })
    return true;
  },
  fetchAll2: (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('participants', params);
    // Select field to populate.
    const populate = Participants.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Participants
      .find()
      .where(filters.where)
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },



  fetchByParam: (params) => {

    /*
      Doc:
        use to get participant by email or mobile
        - In use @AdminPanel and @WP to check email/mobile exists or not
    */

    const populate = Participants.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Participants.findOne({ ...params, is_active: true }).populate(populate);
  },

  /**
   * Promise to fetch a/an participants.
   *
   * @return {Promise}
   */

  fetch: (params) => {
    // Select field to populate.
    const populate = Participants.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Participants
      .findOne(_.pick(params, _.keys(Participants.schema.paths)))
      .populate(populate);
  },

  /**
   * Promise to count participants.
   *
   * @return {Promise}
   */

  count: (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('participants', params);

    return Participants
      .count()
      .where(filters.where);
  },

  /**
   * Promise to add a/an participants.
   *
   * @return {Promise}
   */

  add: async (values) => {

    /*
      Doc:
        Add new participant
        participant_id (uid) = is used as unique id for each participant
        _participant = check if participant exists by mobile
                        if exists return that record
                        else create new
        remaining_seats = used to reduce available seat by 1 on each new registration

        - In use @WP with proceed to check out button.
    */

    // Extract values related to relational data.
    const relations = _.pick(values, Participants.associations.map(ast => ast.alias));
    const data = _.omit(values, Participants.associations.map(ast => ast.alias));


    // Tz-Id
    await storage.init();
    let participant_id = await storage.getItem('participant_id')
    if (participant_id == undefined) {
      participant_id = 2001
      await storage.setItem('participant_id', participant_id)
    } else {
      await storage.setItem('participant_id', participant_id + 1)
    }

    data['uid'] = participant_id

    const populate = Participants.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    data.email = (data.email).toLowerCase()
    console.log(data, "Data in add participant:api")

    let _participant = await Participants.findOne({ mobile: data.mobile, is_active: true }).populate(populate)

    if (_participant) {
      return _participant
    }


    // TODO: comment following block of code to activate remaining seat on transaction
    // also un-comment it from transaction creation

    try {
      // Remaining seats
      let remaining_seats = await storage.getItem('remaining_seats')
      if (remaining_seats == undefined) {
        await storage.setItem('remaining_seats', 50)
      } else {
        // await storage.setItem('remaining_seats', remaining_seats - 1)
      }
    } catch (error) {
      console.log('remaining seats error', error)
    }

    const entry = await Participants.create(data)
    // Create relational data and return the entry.
    return Participants.updateRelations({ _id: entry.id, values: relations });
  },


  addPT: async (values, options, config) => {

    /*
      Doc:
        tz_id (uid) = add/update uid with TZ-**** id
        create participant
        create transaction
        send email and sms

        - In use @AdminPanel at Add Participant page.
    */

    // Extract values related to relational data.
    const relations = _.pick(values, Participants.associations.map(ast => ast.alias));
    const data = _.omit(values, Participants.associations.map(ast => ast.alias));

    // Tz-Id
    await storage.init();
    let tz_id = await storage.getItem('tz_id')
    if (tz_id == undefined) {
      tz_id = 2001
      await storage.setItem('tz_id', tz_id)
    } else {
      await storage.setItem('tz_id', tz_id + 1)
    }

    data.par['uid'] = `TZ-${tz_id}`
    console.log(data, "In addPT, participants services")
    const entry = await Participants.create(data.par)

    data.trx.participants = entry._id
    const trx = await Transaction.create(data.trx)

    try {
      // Remaining seats
      let remaining_seats = await storage.getItem('remaining_seats')
      if (remaining_seats == undefined) {
        await storage.setItem('remaining_seats', 50)
      } else {
        // await storage.setItem('remaining_seats', remaining_seats - 1)
      }
    } catch (error) {
      console.log('remaining seats error', error)
    }

    let op = options.text

    const populate = Participants.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');
    let _participant = await Participants.findOne({ _id: entry._id }).populate(populate)
    let venue_date = _participant.examvenue ? _participant.examvenue.name.split('-') : ["", ""]

    let textMsg = op.replace("tz_uid", `TZ-${tz_id}`).replace('[venue]', venue_date[0]).replace('[date]', venue_date[1])
    delete options.text
    options.html = textMsg

    await strapi.plugins.email.services.email.send(options, config);

    // const user = 'techzillaindia'
    // const pass = '123456'
    // const senId = 'TZILLA'

    const nbs = data.par.mobile
    let sms = data.sms
    const txt = sms.replace("tz_uid", `TZ-${tz_id}`)

    // const uri = encodeURI(`http://bhashsms.com/api/sendmsg.php?user=${user}&pass=${pass}&sender=${senId}&phone=${nbs}&text=${txt}&priority=ndnd&stype=normal`)
    const uri = encodeURI(`http://api.msg91.com/api/sendhttp.php?country=91&sender=TZILLA&route=4&mobiles=${nbs}&authkey=256197A9qclHWiyea5c386431&message=${txt}`)


    axios.get(uri).then(res => {
      // console.log(res)
    }).catch(err => {
      // console.log(err)
    })

    // Create relational data and return the entry.
    return Participants.updateRelations({ _id: entry.id, values: relations });
  },

  /**
   * Promise to edit a/an participants.
   *
   * @return {Promise}
   */

  edit: async (params, values) => {
    // Extract values related to relational data.
    const relations = _.pick(values, Participants.associations.map(a => a.alias));
    const data = _.omit(values, Participants.associations.map(a => a.alias));


    if (data.email) {
      data.email = (data.email).toLowerCase()
    }
    console.log(data, "before Participant update")

    // Update entry with no-relational data.
    const entry = await Participants.update(params, data, { multi: true });

    // console.log("Participant update >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    // console.log(params, "Params >>>>>>>>>>>")
    // console.log(values, "values >>>>>>>>>>>>")
    // console.log(relations, "Relations >>>>>>>>>>>>>")

    // Update relational data and return the entry.
    return Participants.updateRelations(Object.assign(params, { values: relations }));
  },

  /**
   * Promise to remove a/an participants.
   *
   * @return {Promise}
   */

  remove: async params => {
    // Select field to populate.
    const populate = Participants.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    // Note: To get the full response of Mongo, use the `remove()` method
    // or add spent the parameter `{ passRawResult: true }` as second argument.
    const data = await Participants
      .findOneAndRemove(params, {})
      .populate(populate);

    if (!data) {
      return data;
    }

    await Promise.all(
      Participants.associations.map(async association => {
        if (!association.via || !data._id) {
          return true;
        }

        const search = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: data._id } : { [association.via]: { $in: [data._id] } };
        const update = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: null } : { $pull: { [association.via]: data._id } };

        // Retrieve model.
        const model = association.plugin ?
          strapi.plugins[association.plugin].models[association.model || association.collection] :
          strapi.models[association.model || association.collection];

        return model.update(search, update, { multi: true });
      })
    );

    return data;
  },

  /**
   * Promise to search a/an participants.
   *
   * @return {Promise}
   */

  search: async (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('participants', params);
    // Select field to populate.
    const populate = Participants.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    const $or = Object.keys(Participants.attributes).reduce((acc, curr) => {
      switch (Participants.attributes[curr].type) {
        case 'integer':
        case 'float':
        case 'decimal':
          if (!_.isNaN(_.toNumber(params._q))) {
            return acc.concat({ [curr]: params._q });
          }

          return acc;
        case 'string':
        case 'text':
        case 'password':
          return acc.concat({ [curr]: { $regex: params._q, $options: 'i' } });
        case 'boolean':
          if (params._q === 'true' || params._q === 'false') {
            return acc.concat({ [curr]: params._q === 'true' });
          }

          return acc;
        default:
          return acc;
      }
    }, []);

    return Participants
      .find({ $or })
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },

  deleteParticipantsAndRelations: async (body) => {

    /*
      Doc:
      * Multiple
      Delete coupon code for participant
      Delete transaction for participant
      Delete Participant

      - In use @AdminPanel with delete call
    */

    let coupon = await Couponcode.find({
      owner: { $in: body.ids }
    }).remove()
    let transaction = await Transaction.find({
      participants: { $in: body.ids }
    }).remove()
    let user = await Participants.find({
      _id: { $in: body.ids }
    }).remove()
    return { error: false, coupon, transaction, user }

  },


  remainingSeats: async () => {

    /*
      Doc:
        Show remaining seat number
        - In use @WP
    */

    await storage.init()
    let remaining_seats = await storage.getItem('remaining_seats')
    if (remaining_seats == undefined) {
      await storage.setItem('remaining_seats', 50)
    }
    let data = {
      num: await storage.getItem('remaining_seats')
    }
    return data
  },

  getTimeSlotCount: async () => {

    /*
      Doc:
        Used to keep track of available seats for 1st and 2nd batch
        - In use @WP hall ticket page
    */

    await storage.init()
    let f_half_available = await storage.getItem('1st_half')
    let s_half_available = await storage.getItem('2nd_half')
    if (f_half_available == undefined) { await storage.setItem('1st_half', 2) }
    if (s_half_available == undefined) { await storage.setItem('2nd_half', 2) }

    let count = {
      '1st_available': f_half_available,
      '2nd_available': s_half_available
    }

    count['1st'] = await Participants.find({ time_slots: '1st' }).count()
    count['2nd'] = await Participants.find({ time_slots: '2nd' }).count()

    return count

  },

  participant_filter: async (body) => {
    /*
      Doc:
        return Paginated data
        
        buildQuery return array of object 
        each includes valid mongodb query
        for aggregate function

        - In use @AdminPanel
    */

    let query = buildQuery(body)

    query.push({ $skip: parseInt(body.currentIndex) })
    query.push({ $limit: parseInt(body.numberOfRows) })


    let participants = await Participants.aggregate([
      ...query
    ])

    return participants
  },

  participant_filter_count: async (body) => {

    /*
    Doc:
      return Number of records
      
      buildQuery return array of object 
      each includes valid mongodb query
      for aggregate function

      - In use @AdminPanel
  */

    // Add Venue to existed participants (Run Only once) (Required step in order to make available data valid)
    // Uncomment following block of code
    //  Ran once.................
    // Participants.updateMany(
    //   { createdAt: { $lt: Date.now() } },
    //   {
    //     exam: mongoose.Types.ObjectId(''),
    //     examvenue: mongoose.Types.ObjectId('')
    //   },
    // ).exec((err, data) => {
    //   console.log('-----------------------------------')
    //   console.log(err, data)
    //   console.log('-----------------------------------')
    // })

    let query = buildQuery(body)

    let participant = await Participants.aggregate([
      ...query,
      {
        $count: "count"
      }
    ])

    // console.log(participant, '============')
    let count = 0
    if (participant.length > 0) {
      count = participant[0].count
    }
    return { count }
  },

  getResultM: async (param) => {
    if (param.tzid == '' || param.dig == '') {
      return false;
    } else if (param.dig.length > 4 || param.dig.length < 4 || /[a-zA-Z!@#$%^&*(),.?":{}|<>]/.test(param.dig)) {
      return false;
    } else if (/[a-zA-Z!@#$%^&*(),.?":{}|<>]/.test(param.tzid)) {
      return false;
    }

    let data = await Participants.findOne({ "uid": "TZ-" + param.tzid });
    if (!data) {
      return false;
    }
    else { // participants exist
      if (data.is_active) {
        if (data.mobile.substring(6, 10) == param.dig) {
          let result = await Res.findOne({ participants: data._id })
          //let percentile_r = await Res.aggregate([{ $sort: { 'total': -1 } }, { $project: { total: 1, result: { $lt: ["$total", result.total] } } }, { $match: { "result": true } }, { $count: "p_lt" }])
          //let p_total = await Res.find().estimatedDocumentCount();
          //let percentile = Math.trunc((percentile_r[0].p_lt / p_total) * 100)
          return { apti: result.apti, verb: result.verb, tech: result.tech, total: result.total, user: data }
        }
      }
      else {
        return { message: 'Your Exam batch has already seen results, only showing results for new batch now' }
      }

    }

    return false
  },

  updateManyWithTZId: async (params, values) => {

    // return true;
    return await Participants.updateMany({ uid: values.updateTZIdArray }, { testnameMongoId: mongoose.Types.ObjectId(params._id) }, function (err) {
      console.log(err, "Error in participants updateManyWithTZID")
    });

  },
  updateMany: async (params, values) => {

    // return true;
    return await Participants.updateMany({ _id: values.update }, { testnameMongoId: mongoose.Types.ObjectId(params._id) }, function (err) {
      console.log(err, "Error in participants updateMany")
    });

  },

  participantsShowExamButton: async (values) => {

    console.log(values)
    return await Participants.updateMany({ _id: values.updateTZIdArray }, { showStartExamButton: true, timeremain: values.testTimeGiven }, function (err) {
      console.log(err, "Error in participants participantsShowExamButton")
      console.log(err)
    });

  },

  updateManyRemoveTransactions: async (params, values) => {

    // console.log(params._id, values)

    // return true;
    return await Participants.updateMany({ transactions: { $ne: null } }, { transactions: null }, function (err) {
      console.log(err, "Error in participants updateMany remove transactions")
    });

  },
  mark_all_inactive: async (body) => {
    /*
      Doc:
        mark the all participant inactive 
        for given event so they 
        could register again for next event
    */
    return await Participants.updateMany({ exam: body.exam }, { is_active: body.is_active })
  }
};


const buildQuery = (body) => {
  /*
    Doc:
      build query helps to dynamically set pipeline 
      stages for pagination
  */

  /* 
    Collection names:
    colleges
    couponcode
    participants
    transactions
  */


  let query = []

  // Exam Filter @AdminPanel

  if (body.exam && body.exam != 'Select') {
    query.push({
      $match: { "exam": mongoose.Types.ObjectId(body.exam) }
    })
  }

  if (body.testSeriesFilterSelected && body.testSeriesFilterSelected != 'NoSet') {
    query.push({
      $match: { "testnameMongoId": mongoose.Types.ObjectId(body.testSeriesFilterSelected) }
    })
  }

  // if (body.venue != 'Select') {
  //   query.push({
  //     $match: { "examvenue": mongoose.Types.ObjectId(body.venue) }
  //   })
  // }

  // Attended Filter @AdminPanel
  if (body.filter == 'Attended') {
    // get only attended participants
    query.push({ $match: { attended: true } })
  }



  // Join transaction and res collection
  query.push(
    // Sort in desc order
    { $sort: { '_id': -1 } },
    // Join participant
    {
      $lookup: {
        from: 'transactions',
        localField: '_id',
        foreignField: 'participants',
        as: 'transaction'
      }
    },
    // Join res
    {
      $lookup: {
        from: 'res',
        localField: '_id',
        foreignField: 'participants',
        as: 'res'
      }
    },
    {
      $lookup: {
        from: 'answers',
        localField: '_id',
        foreignField: 'userId',
        as: 'answers'
      }
    },
    // Join Test series
    {
      $lookup: {
        from: 'testname',
        localField: 'testnameMongoId',
        foreignField: '_id',
        as: 'testnameMongoId'
      }
    }
  )


  if (body.filter == 'Pending') {
    // console.log('-------------------')
    query.push({ $match: { transaction: { $eq: [] } } })
  }

  /*
     when transaction data is requested 
     we add this extra parameter to get those
     participants who has made transactions
     (Basically it removes those participant 
      who's $transaction is an empty array)
  */
  if (body.filter == 'Transactions') {
    query.push({ $unwind: '$transaction' })
  }

  // Join colleges
  query.push({
    $lookup: {
      from: 'colleges',
      localField: 'colleges',
      foreignField: '_id',
      as: 'college'
    }
  })

  query.push({ $unwind: '$college' })

  // join coupon code
  query.push({
    $lookup: {
      from: 'couponcode',
      localField: '_id',
      foreignField: 'owner',
      as: 'couponCode'
    }
  })


  let matchQuery = { $match: { $or: [] } }

  // Search feature for table
  if (body.search_text != undefined && body.search_text != '') {

    // search query with 'or' condition
    let txt = body.search_text.trim()
    matchQuery.$match.$or.push({ 'name': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'email': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'mobile': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'city': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'state': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'uid': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'college.name': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'couponCode.unique_code': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'transaction.applied_code': { $regex: txt, $options: 'i' } })
    matchQuery.$match.$or.push({ 'time_slots': { $regex: txt, $options: 'i' } })
    query.push(matchQuery)
  }

  // status and time slots filters
  if (body.filter != 'Students'
    && body.filter != 'Transactions'
    && body.filter != "Attended"
    && body.filter != 'Pending') {

    query.push({
      $match: {
        $or: [
          { "transaction.status": { $regex: body.filter, $options: 'i' } },
          { "time_slots": { $regex: body.filter, $options: 'i' } }
        ]
      }
    })
  }


  return query
}

const getTemplate = (Name) => {
  return `
    <!DOCTYPE html
      PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <title>Neopolitan Confirm Email</title>
          <!-- Designed by https://github.com/kaytcat -->
          <!-- Robot header image designed by Freepik.com -->

          <style type="text/css">
              @import url(http://fonts.googleapis.com/css?family=Droid+Sans);

              /* Take care of image borders and formatting */

              * {
                  line-height: 1.5;
              }

              p {
                  font-size: 16px !important
              }

              li {
                  font-size: 14px;

              }

              img {
                  max-width: 600px;
                  outline: none;
                  text-decoration: none;
                  -ms-interpolation-mode: bicubic;
              }

              a {
                  text-decoration: none;
                  border: 0;
                  outline: none;
                  color: #bbbbbb;
              }

              a img {
                  border: none;
              }

              /* General styling */

              td,
              h1,
              h2,
              h3 {
                  font-family: Helvetica, Arial, sans-serif;
                  font-weight: 400;
              }

              td {
                  text-align: center;
              }

              body {
                  -webkit-font-smoothing: antialiased;
                  -webkit-text-size-adjust: none;
                  width: 100%;
                  height: 100%;
                  color: #37302d;
                  background: #ffffff;
                  font-size: 16px;
              }

              table {
                  border-collapse: collapse !important;
              }

              .headline {
                  color: #ffffff;
                  font-size: 36px;
              }

              .force-full-width {
                  width: 100% !important;
              }

              .force-width-80 {
                  width: 80% !important;
              }

              .black-color {
                  color: black;
              }
          </style>

          <style type="text/css" media="screen">
              @media screen {

                  /*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
                  td,
                  h1,
                  h2,
                  h3 {
                      font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                  }
              }
          </style>

          <style type="text/css" media="only screen and (max-width: 480px)">
              /* Mobile styles */
              @media only screen and (max-width: 480px) {

                  table[class="w320"] {
                      width: 320px !important;
                  }

                  td[class="mobile-block"] {
                      width: 100% !important;
                      display: block !important;
                  }


              }
          </style>
      </head>

      <body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none"
          bgcolor="#ffffff">
          <table align="center" cellpadding="0" cellspacing="0" class="force-full-width" height="100%">
              <tr>
                  <td align="center" valign="top" bgcolor="#ffffff" width="100%">
                      <center>
                          <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="600" class="w320">
                              <tr>
                                  <td align="center" valign="top">

                                      <table style="margin: 0 auto;background: #010e28" cellpadding="0" cellspacing="0"
                                          class="force-full-width" style="margin:0 auto;">
                                          <tr>
                                              <td style="font-size: 15px; text-align:center;">
                                                  <br>
                                                  <img src="https://techzillaindia.com/template_images/upskill.png"
                                                      height="50" width="160" alt="">
                                                  <br>
                                                  <br>
                                              </td>
                                          </tr>
                                      </table>

                                      <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width"
                                          bgcolor="#4dbfbf">
                                          <tr>
                                              <td>
                                                  <br>
                                                  <img src="https://techzillaindia.com/template_images/robomail.gif"
                                                      width="224" height="240" alt="robot picture">
                                              </td>
                                          </tr>
                                          <!-- <tr>
                                              <td class="headline">
                                                  Successfully Enrolled
                                              </td>
                                          </tr> -->
                                          <tr>
                                              <td>

                                                  <center>
                                                      <table style="margin: 0 auto;" cellpadding="0" cellspacing="0"
                                                          width="75%">
                                                          <tr>
                                                              <td style="color:#083d3d;text-align: left;padding:5px;">
                                                                  <br>

                                                                  <p>
                                                                      Hey
                                                                      <span class="black-color">
                                                                          <b>
                                                                              ${Name},
                                                                          </b>
                                                                      </span>
                                                                  </p>

                                                                  <p>
                                                                      Your application for NUET is incomplete.
                                                                      <br>
                                                                      Please complete your registration <a target="_blank" class="black-color"
                                                                          href="https://techzillaindia.com/upskill/registration/">
                                                                          <b>here</b>.
                                                                      </a>
                                                                  </p>

                                                                  <div>
                                                                      <p>
                                                                          Why National Upskill Programme is different from
                                                                          others?
                                                                      </p>
                                                                      <ul>
                                                                          <li>
                                                                              No Upfront Fees - No fees are paid during the
                                                                              admission. the training cost will be on us until
                                                                              you get the job.
                                                                          </li>
                                                                          <li>
                                                                              Guaranteed Jobs- We provide guaranteed job
                                                                              placements with a package of minimum 5 LPA upto
                                                                              12LPA.
                                                                          </li>
                                                                          <li>
                                                                              Guaranteed Job PLacement- You will get 100%
                                                                              placement opportunity. If you don't get the job,
                                                                              which is a very rare case, then the training
                                                                              will be free and the training cost we will be
                                                                              upon us.
                                                                          </li>
                                                                          <li>
                                                                              Get trained by the Industry Experts - You will
                                                                              get trained by the Industry experts on the
                                                                              latest technologies and also work on a few live
                                                                              projects.
                                                                          </li>
                                                                      </ul>
                                                                  </div>

                                                                  <div>
                                                                      <p>
                                                                          <b>
                                                                              Why application fees?
                                                                          </b>
                                                                      </p>
                                                                      <ul>
                                                                          <li>
                                                                              The seats are very limited & there are so many
                                                                              applicants. We can’t keep the test open to all
                                                                              as the training is also done at no upfront fees
                                                                              wherein our company will be investing after each
                                                                              candidate.
                                                                          </li>
                                                                          <li>
                                                                              All the colleges take application fees from
                                                                              student & seat is not guaranteed. Even if you
                                                                              get a seat you have to pay upfront fees & no
                                                                              assurance of Job, whereas National Upskill
                                                                              Programme guarantees Min 5LPA & Avg. 7LPA.
                                                                              package.
                                                                          </li>
                                                                          <li>
                                                                              If you apply for MS in the US, the application
                                                                              fees are <br> Min. 100USD and after that, the
                                                                              fees
                                                                              are
                                                                              around 40L.
                                                                          </li>
                                                                          <li>
                                                                              Training institutes take upfront fees for the
                                                                              course which have hardly 20% practical knowledge
                                                                              & provide NO job assurance.
                                                                          </li>
                                                                          <li>
                                                                              An average of all the Entrance test in the
                                                                              market like GRE, CAT, NMAT, TOEFL, etc is INR
                                                                              3000.
                                                                          </li>
                                                                      </ul>
                                                                  </div>
                                                                  <!-- <hr> -->
                                                                  <a href="https://youtu.be/PBBd9aWGyvs" style="width:90%;">
                                                                    <img src="https://techzillaindia.com/upskill/UpskillVideo.jpg" alt="UpskillVideo"/>
                                                                  </a>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </center>

                                              </td>
                                          </tr>
                                      </table>


                                      <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width"
                                          bgcolor="#414141" style="margin: 0 auto">
                                          <tr>
                                              <td style="color:#bbbbbb; font-size:15px">
                                                  <br>
                                                  <span style="text-align: center;">
                                                      For any further assistance email us at <b
                                                          style="color:black">upskill@techzillaindia.com</b>.
                                                  </span>
                                              </td>
                                          </tr>

                                          <tr>

                                              <td style="color:#bbbbbb; font-size:12px;">
                                                  <br />
                                                  <span>National UpSkill program by Techzilla India Infotech</span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td style="color:#bbbbbb; font-size:12px;">
                                                  <br />
                                                  © 2019 All Rights Reserved
                                                  <br>
                                                  <br>
                                                  <br>
                                              </td>
                                          </tr>
                                      </table>
                                  </td>
                              </tr>
                          </table>
                      </center>
                  </td>
              </tr>
          </table>
      </body>

      </html>`
}

const titleCase = (str) => {
  var splitStr = str.toLowerCase().split(' ');
  for (var i = 0; i < splitStr.length; i++) {
    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  return splitStr.join(' ');
}

const getFirstName = (str) => {

  var name = str.split(" ");
  return name[0]
}