'use strict';
// var storage = require('node-persist')
var axios = require('axios')
const http = require('http')
const sgMail = require('@sendgrid/mail');
// sgMail.setApiKey('SG.MlgbVboNSY-vhOEY6Mr0cg.Re0yWYz6EWhoxM42HQWH0IxsIUMycEyci0gqHMUSIaQ') // Local key
sgMail.setApiKey('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') // Live Key
/**
 * Participants.js controller
 *
 * @description: A set of functions called "actions" for managing `Participants`.
 */

module.exports = {

  /**
   * Retrieve participants records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.participants.search(ctx.query);
    } else {
      return strapi.services.participants.fetchAll(ctx.query);
    }
  },

  findAll: async (ctx) => {
    return strapi.services.participants.fetchAll2(ctx.query);
  },
  findAllWithTransactionRelation: async (ctx) => {

    return strapi.services.participants.findAllWithTransactionRelation(ctx.query);

  },

  /**
   * Retrieve a participants record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.participants.fetch(ctx.params);
  },


  findOneByEmailOrNumber: async (ctx) => {

    return strapi.services.participants.fetchByParam(ctx.request.body);;
  },


  otp: async (ctx) => {
    /*
      Doc:
        Get OTP
        - In use @WP
    */
    let url = `http://control.msg91.com/api/sendotp.php?authkey=256197A9qclHWiyea5c386431&sender=TZILLA&mobile=${'91' + ctx.request.body.number}&message=##OTP##`
    if (ctx.request.body.email != undefined) {
      url = `http://control.msg91.com/api/sendotp.php?authkey=256197A9qclHWiyea5c386431&sender=TZILLA&mobile=${'91' + ctx.request.body.number}&email=${ctx.request.body.email}&template=658&message=##OTP##`
    }
    var res = await axios.post(url, {})
    const { data } = await res
    console.log(data)
    return data
  },

  verifyOtp: async (ctx) => {

    /*
      Doc:
        Verify OTP
        - In use @WP
    */

    let url = `https://control.msg91.com/api/verifyRequestOTP.php?authkey=256197A9qclHWiyea5c386431&mobile=${'91' + ctx.request.body.number}&otp=${ctx.request.body.otp}`

    var res = await axios.post(url, {})
    const { data } = await res
    // console.log(data)
    return data

  },

  getCall: async (ctx) => {

    /*
      Doc:
        OTP on call
        - In use @WP
    */

    let url = `http://control.msg91.com/api/retryotp.php?authkey=256197A9qclHWiyea5c386431&mobile=${'91' + ctx.request.body.number}&retrytype=voice`

    let headers = {
      "content-type": "application/x-www-form-urlencoded"
    }

    var res = await axios.post(url, {}, { headers: headers })
    const { data } = await res
    // console.log(data)
    return data
  },


  sendSms: (ctx) => {

    /*
      Doc:
        Send sms
        - In use @WP and @AdminPanel
    */

    console.log('called', ctx.request.body)
    // const user = 'techzillaindia'
    // const pass = '123456'
    // const senId = 'TZILLA'
    const nbs = ctx.request.body.mobile
    const txt = ctx.request.body.msg
    const uri = encodeURI(`http://api.msg91.com/api/sendhttp.php?country=91&sender=TZILLA&route=4&mobiles=${nbs}&authkey=256197A9qclHWiyea5c386431&message=${txt}`)

    axios.get(uri).then(res => {
    }).catch(err => {
      console.log(err)
    })

    return {}
  },
  /**
   * Count participants records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.participants.count(ctx.query);
  },

  /**
   * Create a/an participants record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.participants.add(ctx.request.body);
  },

  createPT: async (ctx) => {
    const config = await strapi.store({
      environment: strapi.config.environment,
      type: 'plugin',
      name: 'email'
    }).get({ key: 'provider' });

    // Verify if the file email is enable.
    if (config.enabled === false) {
      strapi.log.error('Email is disabled');
      return ctx.badRequest(null, ctx.request.admin ? [{ messages: [{ id: 'Email.status.disabled' }] }] : 'Emailis disabled');
    }

    // Something is wrong
    if (ctx.status === 400) {
      return;
    }

    let options = ctx.request.body.email;


    return strapi.services.participants.addPT(ctx.request.body, options, config);
  },
  /**
   * Update a/an participants record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.participants.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an participants record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.participants.remove(ctx.params);
  },

  deleteParticipantsAndRelations: async (ctx) => {
    return strapi.services.participants.deleteParticipantsAndRelations(ctx.request.body)
  },


  remainingSeats: async () => {
    return await strapi.services.participants.remainingSeats()
  },

  getTimeSlotCount: async () => {
    return await strapi.services.participants.getTimeSlotCount()
  },

  customSendMail: async (ctx) => {
    /*
      Doc:
        Send custom mail
        - In use @AdminPanel
    */
    const msg = {
      to: ctx.request.body.to,
      from: ctx.request.body.from,
      subject: ctx.request.body.subject,
      html: ctx.request.body.html,
    };

    return await sgMail.sendMultiple(msg);
  },

  participant_filter: async (ctx) => {
    return await strapi.services.participants.participant_filter(ctx.request.body)
  },


  participant_filter_count: async (ctx) => {
    return await strapi.services.participants.participant_filter_count(ctx.request.body)
  },

  getResult: async (ctx) => {
    let data = {
      tzid: ctx.request.query.tzid,
      dig: ctx.request.query.dig
    }
    return await strapi.services.participants.getResultM(data)
  },

  updateMany: async (ctx, next) => {

    return strapi.services.participants.updateMany(ctx.params, ctx.request.body);
  },
  updateManyWithTZId: async (ctx, next) => {

    return strapi.services.participants.updateManyWithTZId(ctx.params, ctx.request.body);
  },

  participantsShowExamButton: async (ctx) => {

    return strapi.services.participants.participantsShowExamButton(ctx.request.body);
  },

  mark_all_inactive: async (ctx) => {
    return await strapi.services.participants.mark_all_inactive(ctx.request.body);
  },

  send_schedule_mails: async () => {
    return await strapi.services.participants.send_schedule_mails()
  },
  updateManyRemoveTransactions: async (ctx, next) => {

    return strapi.services.participants.updateManyRemoveTransactions(ctx.params, ctx.request.body);
  }

};
