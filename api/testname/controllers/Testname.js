'use strict';

/**
 * Testname.js controller
 *
 * @description: A set of functions called "actions" for managing `Testname`.
 */

module.exports = {

  /**
   * Retrieve testname records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.testname.search(ctx.query);
    } else {
      return strapi.services.testname.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a testname record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.testname.fetch(ctx.params);
  },

  /**
   * Count testname records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.testname.count(ctx.query);
  },

  /**
   * Create a/an testname record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.testname.add(ctx.request.body);
  },

  /**
   * Update a/an testname record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.testname.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an testname record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.testname.remove(ctx.params);
  }
};
