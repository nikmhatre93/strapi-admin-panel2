'use strict';

/**
 * Questions.js service
 *
 * @description: A set of functions similar to controller's actions to avoid code duplication.
 */

// Public dependencies.
const _ = require('lodash');
var mongoose = require('mongoose')

module.exports = {

  /**
   * Promise to fetch all questions.
   *
   * @return {Promise}
   */

  fetchAll: (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('questions', params);
    // Select field to populate.
    const populate = Questions.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Questions
      .find()
      .where(filters.where)
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },

  /**
   * Promise to fetch a/an questions.
   *
   * @return {Promise}
   */

  fetch: (params) => {
    // Select field to populate.
    const populate = Questions.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Questions
      .findOne(_.pick(params, _.keys(Questions.schema.paths)))
      .populate(populate);
  },

  /**
   * Promise to count questions.
   *
   * @return {Promise}
   */

  count: (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('questions', params);

    return Questions
      .count()
      .where(filters.where);
  },

  /**
   * Promise to add a/an questions.
   *
   * @return {Promise}
   */

  add: async (values) => {
    // Extract values related to relational data.
    const relations = _.pick(values, Questions.associations.map(ast => ast.alias));
    const data = _.omit(values, Questions.associations.map(ast => ast.alias));
    // console.log(values, "values")
    // console.log(data, "data")
    // Create entry with no-relational data.
    const entry = await Questions.create(data);

    // Create relational data and return the entry.
    return Questions.updateRelations({ _id: entry.id, values: relations });
  },

  /**
   * Promise to edit a/an questions.
   *
   * @return {Promise}
   */

  edit: async (params, values) => {
    // Extract values related to relational data.
    const relations = _.pick(values, Questions.associations.map(a => a.alias));
    const data = _.omit(values, Questions.associations.map(a => a.alias));

    // Update entry with no-relational data.
    const entry = await Questions.update(params, data, { multi: true });

    // Update relational data and return the entry.
    return Questions.updateRelations(Object.assign(params, { values: relations }));
  },

  /**
   * Promise to remove a/an questions.
   *
   * @return {Promise}
   */

  remove: async params => {
    // Select field to populate.
    const populate = Questions.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    // Note: To get the full response of Mongo, use the `remove()` method
    // or add spent the parameter `{ passRawResult: true }` as second argument.
    const data = await Questions
      .findOneAndRemove(params, {})
      .populate(populate);

    if (!data) {
      return data;
    }

    await Promise.all(
      Questions.associations.map(async association => {
        if (!association.via || !data._id) {
          return true;
        }

        const search = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: data._id } : { [association.via]: { $in: [data._id] } };
        const update = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: null } : { $pull: { [association.via]: data._id } };

        // Retrieve model.
        const model = association.plugin ?
          strapi.plugins[association.plugin].models[association.model || association.collection] :
          strapi.models[association.model || association.collection];

        return model.update(search, update, { multi: true });
      })
    );

    return data;
  },

  /**
   * Promise to search a/an questions.
   *
   * @return {Promise}
   */

  search: async (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('questions', params);
    // Select field to populate.
    const populate = Questions.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    const $or = Object.keys(Questions.attributes).reduce((acc, curr) => {
      switch (Questions.attributes[curr].type) {
        case 'integer':
        case 'float':
        case 'decimal':
          if (!_.isNaN(_.toNumber(params._q))) {
            return acc.concat({ [curr]: params._q });
          }

          return acc;
        case 'string':
        case 'text':
        case 'password':
          return acc.concat({ [curr]: { $regex: params._q, $options: 'i' } });
        case 'boolean':
          if (params._q === 'true' || params._q === 'false') {
            return acc.concat({ [curr]: params._q === 'true' });
          }

          return acc;
        default:
          return acc;
      }
    }, []);

    return Questions
      .find({ $or })
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },


  //Fire Socket Event for Exam Start(Admin Panel)
  EventFire: async (params) => {
    console.log(params, "Before emit")
    strapi.io.emit('StartExam', params);

    strapi.socket.on('StartExam', (values) => {
      console.log(values, "After emit, inside .on")
    })

    return { "Status": "true" }

  },

  findAllOfOneTestSet: (values) => {

    return Questions.aggregate(
      [
        { "$match": { "testnameMongoId": mongoose.Types.ObjectId(values._id) } },
        {
          $lookup:
          {
            from: "questiontype",
            localField: "questiontype",
            foreignField: "_id",
            as: "questiontype",
          }
        }
        ,
        {
          $unwind: { "path": "$questiontype" }
        }
        ,
        { $sort: { "questiontype.SortKey": 1 } }

      ]
    )
  },

  restoreQuestionsParticularTestSet: (values) => {

    // console.log(values)

    return Questions.aggregate(
      [
        { "$match": { "testnameMongoId": mongoose.Types.ObjectId(values.testSeriesId) } },

        {
          $lookup:
          {
            from: "questiontype",
            localField: "questiontype",
            foreignField: "_id",
            as: "questiontype",
          }
        }
        ,
        {
          $unwind: { "path": "$questiontype" }
        }
        ,

        {
          $lookup:
          {
            from: "answers",
            localField: "_id",
            foreignField: "questionId",
            as: "answeredquestions",
          }
        },

        { $sort: { "questiontype.SortKey": 1 } },

        {
          $project:
          {
            ActualQuestion: 1,
            options: 1,
            Answer: 1,
            questiontype: 1,
            questionimage: 1,
            answeredquestions:
            {
              $filter:
              {
                input: "$answeredquestions",
                as: "answer",
                cond: { $eq: ["$$answer.userId", mongoose.Types.ObjectId(values.participantsId)] }
              }
            },
            id: 1,
          }

        },

      ]
    )
  },

  // Used to get all Questions Section wise and the question count,name and id of section on top.
  findAll: (params) => {

    // Group By Question Type ID.Sort by Sort Key(Priority).Questions is an array of objects.
    // If we get the length of the inital array,we will get the no of sections.Also included is the 
    // count of questions per section using group count.

    let querykey = params.querykey

    console.log(querykey)

    if (params.querykey == "nouser") {
      return Questions.aggregate(
        [
          {
            $lookup:
            {
              from: "questiontype",
              localField: "questiontype",
              foreignField: "_id",
              as: "questiontype",
            }
          }
          ,
          {
            $unwind: { "path": "$questiontype" }
          }
          ,
          {
            $lookup:
            {
              from: "testname",
              localField: "testnameMongoId",
              foreignField: "_id",
              as: "testnameMongoId",
            }
          }
          ,
          {
            $unwind: { "path": "$testnameMongoId" }
          }
          ,
          {
            $lookup:
            {
              from: "upload_file",
              // localField: "_id",
              // foreignField: "related",
              let: { id: "$_id" },
              pipeline: [
                {
                  $unwind: { "path": "$related" }
                },
                {
                  $match:
                  {
                    $expr: {
                      $and:
                        [
                          { $eq: ["$related.ref", "$$id"] }]
                    }
                  }
                }
              ],
              as: "questionimage",
            }
          },

          // {
          //   $lookup:
          //   {
          //     from:"upload_file",
          //     localField:"_id",
          //     foreignField:"_id",
          //     as:"questiontype",
          //   }
          // },

          // {
          //   $group:
          //   {
          //     _id:"$questiontype.SortKey",
          //     QuestionsCount:{$sum: 1},
          //     SectionClass:{"$first":"$questiontype.questiontype"},
          //     SectionId:{"$first":"$questiontype._id"},
          //     Questions: { $push: "$$ROOT" },
          //   }
          // },

          { $sort: { "questiontype.SortKey": 1 } }

        ])
    }

    else {
      return Questions.aggregate(
        [
          {
            $lookup:
            {
              from: "questiontype",
              localField: "questiontype",
              foreignField: "_id",
              as: "questiontype",
            }
          }
          ,
          {
            $unwind: { "path": "$questiontype" }
          }
          ,

          {
            $lookup:
            {
              from: "answers",
              localField: "_id",
              foreignField: "questionId",
              as: "answeredquestions",
            }
          },

          {
            $lookup:
            {
              from: "upload_file",
              // localField: "_id",
              // foreignField: "related",
              let: { id: "$_id" },
              pipeline: [
                {
                  $unwind: { "path": "$related" }
                },
                {
                  $match:
                  {
                    $expr: {
                      $and:
                        [
                          { $eq: ["$related.ref", "$$id"] }]
                    }
                  }
                }
              ],
              as: "questionimage",
            }
          },

          { $sort: { "questiontype.SortKey": 1 } },

          {
            $project:
            {
              ActualQuestion: 1,
              options: 1,
              Answer: 1,
              questiontype: 1,
              questionimage: 1,
              answeredquestions:
              {
                $filter:
                {
                  input: "$answeredquestions",
                  as: "answer",
                  cond: { $eq: ["$$answer.userId", mongoose.Types.ObjectId(params.querykey)] }
                }
              },
              id: 1,
            }

          },

        ])
    }
    // ,(err,result) =>
    // {
    //   console.log("RESULT")
    //   console.log(result)
    //   // result.populate({path:'questiontype'},(err,data)=>
    //   // {
    //   //   console.log(data)
    //   //   console.log(err)
    //   // })
    // })

    //The Aim is to sort the data by population the questiontype field and using the SortKey field in this question
    // type to arrange the data in an ascending order.Then on this output,use group by to group all related question
    // types together(Using Aggregate or Lookup?) and then send this data to the frontend side.
  },
  insertManyTestSeries: async (params) => {
    // console.log(params.Questions,"Insert many")
    // return true
    return await Questions.insertMany(params.Questions)
    // .then(function (docs) {
    //     // response.json(docs);
    //     console.log(docs,"Success")
    // })
    // .catch(function (err) {
    //     // response.status(500).send(err);
    //     console.log(err,"Error")
    // });
  },
  questions_filter_count: async (params) => {

    const query = questionsBuildQuery(params);
    // console.log(query)

    let questions = await Questions.aggregate([
      ...query,
      {
        $count: "count"
      }
    ])

    let count = 0
    if (questions.length > 0) {
      count = questions[0].count
    }
    return { count }

  },
  questions_filter: async (params) => {

    const query = questionsBuildQuery(params);
    // console.log(query)
    query.push({ $skip: parseInt(params.currentIndex) })
    query.push({ $limit: parseInt(params.numberOfRows) })

    let questions = await Questions.aggregate([
      ...query
    ])

    return questions

  },
  deleteManyQuestions: async (body) => {

    let questions_deleted = await Questions.find({
      _id: { $in: body.deleteQuestionIds }
    }).remove()
    return { error: false, questions_deleted }
  }
};

const questionsBuildQuery = (params) => {
  // return params;
  let query = []


  if (params.questionTypeSelected && params.questionTypeSelected != 'NoSet') {
    query.push({
      $match: { "questiontype": mongoose.Types.ObjectId(params.questionTypeSelected) }
    })
  }

  if (params.testSeriesSelected && params.testSeriesSelected != 'NoSet') {
    query.push({
      $match: { "testnameMongoId": mongoose.Types.ObjectId(params.testSeriesSelected) }
    })
  }

  query.push({
    $lookup: {
      from: 'questiontype',
      localField: 'questiontype',
      foreignField: '_id',
      as: 'questiontype'
    }
  })

  query.push({ $unwind: '$questiontype' })

  query.push({
    $lookup:
    {
      from: "testname",
      localField: "testnameMongoId",
      foreignField: "_id",
      as: "testnameMongoId",
    }
  })

  query.push({ $unwind: '$testnameMongoId' })

  query.push({ $sort: { "questiontype.SortKey": 1 } })

  return query;



}