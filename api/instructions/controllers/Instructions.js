'use strict';

/**
 * Instructions.js controller
 *
 * @description: A set of functions called "actions" for managing `Instructions`.
 */

module.exports = {

  /**
   * Retrieve instructions records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.instructions.search(ctx.query);
    } else {
      return strapi.services.instructions.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a instructions record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.instructions.fetch(ctx.params);
  },

  /**
   * Count instructions records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.instructions.count(ctx.query);
  },

  /**
   * Create a/an instructions record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.instructions.add(ctx.request.body);
  },

  /**
   * Update a/an instructions record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.instructions.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an instructions record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.instructions.remove(ctx.params);
  }
};
