'use strict';

/**
 * Answers.js service
 *
 * @description: A set of functions similar to controller's actions to avoid code duplication.
 */

// Public dependencies.
const _ = require('lodash');
var mongoose = require('mongoose');

module.exports = {

  /**
   * Promise to fetch all answers.
   *
   * @return {Promise}
   */

  fetchAll: (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('answers', params);
    // Select field to populate.
    const populate = Answers.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Answers
      .find()
      .where(filters.where)
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },

  /**
   * Promise to fetch a/an answers.
   *
   * @return {Promise}
   */

  fetch: (params) => {
    // Select field to populate.
    const populate = Answers.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Answers
      .findOne(_.pick(params, _.keys(Answers.schema.paths)))
      .populate(populate);
  },

  /**
   * Promise to count answers.
   *
   * @return {Promise}
   */

  count: (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('answers', params);

    return Answers
      .count()
      .where(filters.where);
  },

  /**
   * Promise to add a/an answers.
   *
   * @return {Promise}
   */

  add: async (values) => {
    // Extract values related to relational data.
    const relations = _.pick(values, Answers.associations.map(ast => ast.alias));
    const data = _.omit(values, Answers.associations.map(ast => ast.alias));

    // Create entry with no-relational data.
    const entry = await Answers.create(data);

    // Create relational data and return the entry.
    return Answers.updateRelations({ _id: entry.id, values: relations });
  },

  /**
   * Promise to edit a/an answers.
   *
   * @return {Promise}
   */

  edit: async (params, values) => {
    // Extract values related to relational data.
    const relations = _.pick(values, Answers.associations.map(a => a.alias));
    const data = _.omit(values, Answers.associations.map(a => a.alias));

    // Update entry with no-relational data.
    const entry = await Answers.update(params, data, { multi: true });

    // Update relational data and return the entry.
    return Answers.updateRelations(Object.assign(params, { values: relations }));
  },

  /**
   * Promise to remove a/an answers.
   *
   * @return {Promise}
   */

  remove: async params => {
    // Select field to populate.
    const populate = Answers.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    // Note: To get the full response of Mongo, use the `remove()` method
    // or add spent the parameter `{ passRawResult: true }` as second argument.
    const data = await Answers
      .findOneAndRemove(params, {})
      .populate(populate);

    if (!data) {
      return data;
    }

    await Promise.all(
      Answers.associations.map(async association => {
        if (!association.via || !data._id) {
          return true;
        }

        const search = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: data._id } : { [association.via]: { $in: [data._id] } };
        const update = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: null } : { $pull: { [association.via]: data._id } };

        // Retrieve model.
        const model = association.plugin ?
          strapi.plugins[association.plugin].models[association.model || association.collection] :
          strapi.models[association.model || association.collection];

        return model.update(search, update, { multi: true });
      })
    );

    return data;
  },

  /**
   * Promise to search a/an answers.
   *
   * @return {Promise}
   */

  search: async (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('answers', params);
    // Select field to populate.
    const populate = Answers.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    const $or = Object.keys(Answers.attributes).reduce((acc, curr) => {
      switch (Answers.attributes[curr].type) {
        case 'integer':
        case 'float':
        case 'decimal':
          if (!_.isNaN(_.toNumber(params._q))) {
            return acc.concat({ [curr]: params._q });
          }

          return acc;
        case 'string':
        case 'text':
        case 'password':
          return acc.concat({ [curr]: { $regex: params._q, $options: 'i' } });
        case 'boolean':
          if (params._q === 'true' || params._q === 'false') {
            return acc.concat({ [curr]: params._q === 'true' });
          }

          return acc;
        default:
          return acc;
      }
    }, []);

    return Answers
      .find({ $or })
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },

  SaveAns: async (values) => {

    // console.log("data Received",values)

    var FirstSearch = Answers.find({ "questionId": values.QuestionId, "userId": values.userId }, (err, docs) => {
      // console.log(err)
      // console.log(docs)

      if (docs) {
        if (docs.length > 0) {
          var data_sending =
          {
            "SortKey": values.SortKey,
            "timestamp": values.timestamp,
            "questionId": values.QuestionId,
            "userId": values.userId,
            "markedAns": values.Answer,
            "questionNo": values.QuestionNo
          }

          // console.log("Element Exists,update with new object")

          Answers.update({ "questionId": values.QuestionId, "userId": values.userId }, { ...data_sending }).exec((err, docs) => {

          })
        }

        else {
          // console.log("Create with new Object")
          Answers.create({
            "SortKey": values.SortKey,
            "timestamp": values.timestamp,
            "questionId": mongoose.Types.ObjectId(values.QuestionId),
            "userId": mongoose.Types.ObjectId(values.userId),
            "markedAns": values.Answer,
            "questionNo": values.QuestionNo
          })
        }
      }
    })

    return true
  },
  answersDeleteOne: async (values) => {

    var deletedAnswer = await Answers.findOneAndRemove({ "questionId": values.QuestionId, "userId": values.userId })
    return deletedAnswer;

  },

  SortAns: async (values) => {

    // console.log(values._id,"SortAnswer")
    // return Answers.find({})
    return Answers.aggregate(
      [
        {
          $lookup:
          {
            from: "participants",
            localField: "userId",
            foreignField: "_id",
            as: "userId",
          }
        }
        ,
        {
          $unwind: { "path": "$userId" }
        },
        {
          $lookup:
          {
            from: "questions",
            localField: "questionId",
            foreignField: "_id",
            as: "questionId",
          }
        }
        ,
        {
          $unwind: { "path": "$questionId" }
        },
        {
          $lookup:
          {
            from: "questiontype",
            localField: "questionId.questiontype",
            foreignField: "_id",
            as: "sectionTableDeatils",
          }
        },
        {
          $unwind: { "path": "$sectionTableDeatils" }
        }
        ,
        {
          $lookup: {
            from: 'res',
            localField: 'userId._id',
            foreignField: 'participants',
            as: 'userId.res'
          }
        }
        // ,
        // {
        //   $unwind: { "path": "$userId.res" }
        // }
        ,
        {
          $project:
          {

            SortKey: "$SortKey",
            sectionName: "$sectionTableDeatils.questiontype",
            actualAnswer: "$questionId.Answer",
            markedAnswer: "$markedAns",
            candidateName: "$userId.name",
            candidateTzID: "$userId.uid",
            QuestionNo: "$questionNo",
            sectionMongoId: "$sectionTableDeatils._id",
            questionMongoId: "$questionId._id",
            candidateMongoID: "$userId._id",
            Question: "$questionId.ActualQuestion",
            result: "$userId.res"
          }
        },

        { $sort: { "candidateMongoID": 1, "SortKey": 1 } },

      ]
    )

    // return Answers
    //   .find()

  },
  answers_Sorted_testSeries: async (values) => {
    // console.log(values)
    return Answers.aggregate(
      [
        {
          $lookup:
          {
            from: "participants",
            localField: "userId",
            foreignField: "_id",
            as: "userId",
          }
        }
        ,
        {
          $unwind: { "path": "$userId" }
        },
        { "$match": { "userId.testnameMongoId": mongoose.Types.ObjectId(values._id) } },

        {
          $lookup:
          {
            from: "questions",
            localField: "questionId",
            foreignField: "_id",
            as: "questionId",
          }
        }
        ,
        {
          $unwind: { "path": "$questionId" }
        },
        {
          $lookup:
          {
            from: "questiontype",
            localField: "questionId.questiontype",
            foreignField: "_id",
            as: "sectionTableDeatils",
          }
        },
        {
          $unwind: { "path": "$sectionTableDeatils" }
        }
        ,
        {
          $lookup: {
            from: 'res',
            localField: 'userId._id',
            foreignField: 'participants',
            as: 'userId.res'
          }
        }
        ,
        {
          $project:
          {

            SortKey: "$SortKey",
            sectionName: "$sectionTableDeatils.questiontype",
            actualAnswer: "$questionId.Answer",
            markedAnswer: "$markedAns",
            candidateName: "$userId.name",
            candidateTzID: "$userId.uid",
            QuestionNo: "$questionNo",
            sectionMongoId: "$sectionTableDeatils._id",
            questionMongoId: "$questionId._id",
            candidateMongoID: "$userId._id",
            Question: "$questionId.ActualQuestion",
            result: "$userId.res"
          }
        },

        { $sort: { "candidateMongoID": 1, "SortKey": 1 } }

      ])

  },
  OnlyOne: async (values) => {
    // console.log(values._id,"OnlyOne")
    return Answers.aggregate(
      [
        { "$match": { "userId": mongoose.Types.ObjectId(values._id) } },
        {
          $lookup:
          {
            from: "participants",
            localField: "userId",
            foreignField: "_id",
            as: "userId",
          }
        }
        ,
        {
          $unwind: { "path": "$userId" }
        },
        {
          $lookup:
          {
            from: "questions",
            localField: "questionId",
            foreignField: "_id",
            as: "questionId",
          }
        }
        ,
        {
          $unwind: { "path": "$questionId" }
        },
        {
          $lookup:
          {
            from: "questiontype",
            localField: "questionId.questiontype",
            foreignField: "_id",
            as: "sectionTableDeatils",
          }
        },
        {
          $unwind: { "path": "$sectionTableDeatils" }
        },
        {
          $project:
          {

            SortKey: "$SortKey",
            sectionName: "$sectionTableDeatils.questiontype",
            actualAnswer: "$questionId.Answer",
            markedAnswer: "$markedAns",
            candidateName: "$userId.name",
            candidateTzID: "$userId.uid",
            QuestionNo: "$questionNo",
            sectionMongoId: "$sectionTableDeatils._id",
            questionMongoId: "$questionId._id",
            candidateMongoID: "$userId._id",
            Question: "$questionId.ActualQuestion"
          }
        },
        // { "$match": { "candidateMongoID": values._id } },
        { $sort: { "candidateMongoID": 1, "SortKey": 1 } },

      ]
    )
  },
  deleteAllOneUser: async (values) => {

    console.log(values._id)
    // return true;
    return Answers.deleteMany({ userId: mongoose.Types.ObjectId(values._id) }, function (err) {
      console.log(err, "Error")
    });
  }
};