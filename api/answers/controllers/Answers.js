'use strict';

/**
 * Answers.js controller
 *
 * @description: A set of functions called "actions" for managing `Answers`.
 */

module.exports = {

  /**
   * Retrieve answers records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.answers.search(ctx.query);
    } else {
      return strapi.services.answers.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a answers record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.answers.fetch(ctx.params);
  },

  /**
   * Count answers records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.answers.count(ctx.query);
  },

  /**
   * Create a/an answers record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.answers.add(ctx.request.body);
  },

  /**
   * Update a/an answers record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.answers.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an answers record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.answers.remove(ctx.params);
  },

  SaveAns: async (ctx) => {
    return strapi.services.answers.SaveAns(ctx.request.body);
  },

  answersDeleteOne: async (ctx) => {
    return strapi.services.answers.answersDeleteOne(ctx.request.body);
  },

  SortAns: async (ctx) => {
    return strapi.services.answers.SortAns(ctx.params);
  },
  answers_Sorted_testSeries: async (ctx) => {

    return strapi.services.answers.answers_Sorted_testSeries(ctx.params);
  },

  OnlyOne: async (ctx) => {

    return strapi.services.answers.OnlyOne(ctx.params);
  },

  deleteAllOneUser: async (ctx, next) => {

    return strapi.services.answers.deleteAllOneUser(ctx.params);
  },
};
