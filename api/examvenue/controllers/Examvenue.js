'use strict';

/**
 * Examvenue.js controller
 *
 * @description: A set of functions called "actions" for managing `Examvenue`.
 */

module.exports = {

  /**
   * Retrieve examvenue records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.examvenue.search(ctx.query);
    } else {
      return strapi.services.examvenue.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a examvenue record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.examvenue.fetch(ctx.params);
  },

  /**
   * Count examvenue records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.examvenue.count(ctx.query);
  },

  /**
   * Create a/an examvenue record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.examvenue.add(ctx.request.body);
  },

  /**
   * Update a/an examvenue record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.examvenue.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an examvenue record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.examvenue.remove(ctx.params);
  }
};
