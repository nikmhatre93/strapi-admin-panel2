'use strict';

/**
 * Questiontype.js controller
 *
 * @description: A set of functions called "actions" for managing `Questiontype`.
 */

module.exports = {

  /**
   * Retrieve questiontype records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.questiontype.search(ctx.query);
    } else {
      return strapi.services.questiontype.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a questiontype record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.questiontype.fetch(ctx.params);
  },

  /**
   * Count questiontype records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.questiontype.count(ctx.query);
  },

  /**
   * Create a/an questiontype record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.questiontype.add(ctx.request.body);
  },

  /**
   * Update a/an questiontype record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.questiontype.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an questiontype record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.questiontype.remove(ctx.params);
  }
};
