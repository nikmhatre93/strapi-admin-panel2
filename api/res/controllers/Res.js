'use strict';

/**
 * Res.js controller
 *
 * @description: A set of functions called "actions" for managing `Res`.
 */

module.exports = {

  /**
   * Retrieve res records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.res.search(ctx.query);
    } else {
      return strapi.services.res.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a res record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.res.fetch(ctx.params);
  },

  /**
   * Count res records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.res.count(ctx.query);
  },

  /**
   * Create a/an res record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.res.add(ctx.request.body);
  },

  /**
   * Update a/an res record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.res.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an res record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.res.remove(ctx.params);
  },

  insertManyResults: async (ctx) => {
    return await strapi.services.res.insertManyResults(ctx.request.body);
  },
  insertUpdateUpsert: async (ctx, next) => {
    return strapi.services.res.insertUpdateUpsert(ctx.params, ctx.request.body);
  }
};
