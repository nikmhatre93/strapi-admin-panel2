'use strict';

/**
 * Couponcode.js controller
 *
 * @description: A set of functions called "actions" for managing `Couponcode`.
 */

module.exports = {

  /**
   * Retrieve couponcode records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.couponcode.search(ctx.query);
    } else {
      return strapi.services.couponcode.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a couponcode record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.couponcode.fetch(ctx.params);
  },

  /**
   * Count couponcode records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.couponcode.count(ctx.query);
  },

  /**
   * Create a/an couponcode record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.couponcode.add(ctx.request.body);
  },

  /**
   * Update a/an couponcode record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.couponcode.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an couponcode record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.couponcode.remove(ctx.params);
  },

  createCouponCodes: async () => {
    return strapi.services.couponcode.createCouponCodes();
  },

  findByCouponcodes: async (ctx) => {
    return strapi.services.couponcode.findByCouponcodes(ctx.request.body)
  }
};
