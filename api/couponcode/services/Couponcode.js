'use strict';

/**
 * Couponcode.js service
 *
 * @description: A set of functions similar to controller's actions to avoid code duplication.
 */

// Public dependencies.
const _ = require('lodash');

module.exports = {

  /**
   * Promise to fetch all couponcodes.
   *
   * @return {Promise}
   */

  fetchAll: (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('couponcode', params);
    // Select field to populate.
    const populate = Couponcode.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Couponcode
      .find()
      .where(filters.where)
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },

  /**
   * Promise to fetch a/an couponcode.
   *
   * @return {Promise}
   */

  fetch: (params) => {
    // Select field to populate.
    const populate = Couponcode.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    return Couponcode
      .findOne(_.pick(params, _.keys(Couponcode.schema.paths)))
      .populate(populate);
  },

  /**
   * Promise to count couponcodes.
   *
   * @return {Promise}
   */

  count: (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('couponcode', params);

    return Couponcode
      .count()
      .where(filters.where);
  },

  /**
   * Promise to add a/an couponcode.
   *
   * @return {Promise}
   */

  add: async (values) => {
    // Extract values related to relational data.
    const relations = _.pick(values, Couponcode.associations.map(ast => ast.alias));
    const data = _.omit(values, Couponcode.associations.map(ast => ast.alias));

    // Create entry with no-relational data.
    const entry = await Couponcode.create(data);

    // Create relational data and return the entry.
    return Couponcode.updateRelations({ _id: entry.id, values: relations });
  },

  /**
   * Promise to edit a/an couponcode.
   *
   * @return {Promise}
   */

  edit: async (params, values) => {
    // Extract values related to relational data.
    const relations = _.pick(values, Couponcode.associations.map(a => a.alias));
    const data = _.omit(values, Couponcode.associations.map(a => a.alias));

    // Update entry with no-relational data.
    const entry = await Couponcode.update(params, data, { multi: true });

    // Update relational data and return the entry.
    return Couponcode.updateRelations(Object.assign(params, { values: relations }));
  },

  /**
   * Promise to remove a/an couponcode.
   *
   * @return {Promise}
   */

  remove: async params => {
    // Select field to populate.
    const populate = Couponcode.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    // Note: To get the full response of Mongo, use the `remove()` method
    // or add spent the parameter `{ passRawResult: true }` as second argument.
    const data = await Couponcode
      .findOneAndRemove(params, {})
      .populate(populate);

    if (!data) {
      return data;
    }

    await Promise.all(
      Couponcode.associations.map(async association => {
        if (!association.via || !data._id) {
          return true;
        }

        const search = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: data._id } : { [association.via]: { $in: [data._id] } };
        const update = _.endsWith(association.nature, 'One') || association.nature === 'oneToMany' ? { [association.via]: null } : { $pull: { [association.via]: data._id } };

        // Retrieve model.
        const model = association.plugin ?
          strapi.plugins[association.plugin].models[association.model || association.collection] :
          strapi.models[association.model || association.collection];

        return model.update(search, update, { multi: true });
      })
    );

    return data;
  },

  /**
   * Promise to search a/an couponcode.
   *
   * @return {Promise}
   */

  search: async (params) => {
    // Convert `params` object to filters compatible with Mongo.
    const filters = strapi.utils.models.convertParams('couponcode', params);
    // Select field to populate.
    const populate = Couponcode.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');

    const $or = Object.keys(Couponcode.attributes).reduce((acc, curr) => {
      switch (Couponcode.attributes[curr].type) {
        case 'integer':
        case 'float':
        case 'decimal':
          if (!_.isNaN(_.toNumber(params._q))) {
            return acc.concat({ [curr]: params._q });
          }

          return acc;
        case 'string':
        case 'text':
        case 'password':
          return acc.concat({ [curr]: { $regex: params._q, $options: 'i' } });
        case 'boolean':
          if (params._q === 'true' || params._q === 'false') {
            return acc.concat({ [curr]: params._q === 'true' });
          }

          return acc;
        default:
          return acc;
      }
    }, []);

    return Couponcode
      .find({ $or })
      .sort(filters.sort)
      .skip(filters.start)
      .limit(filters.limit)
      .populate(populate);
  },

  createCouponCodes: async () => {
    /* 
      Doc: 
          Not in use anymore
          used to create coupon codes
          for those who made the transaction
          (when coupon code newly introduced)
    */

    let couponCode = await Couponcode.find({}, 'owner')
    let couponHolderList = []
    couponCode.forEach(item => {
      couponHolderList.push(item.owner)
    })
    let participants = await Transaction.find({
      participants: { $nin: couponHolderList },
      status: 'Completed'
    })


    participants.forEach(async (item) => {

      try {

        var text = "TZ";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 6; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));

        let data = {
          owner: item.participants,
          unique_code: text
        }
        await Couponcode.create(data);
      } catch (err) {

        var text = "TZ";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 6; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));

        let data = {
          owner: item.participants,
          unique_code: text
        }
        await Couponcode.create(data);
      }

    })

    return true
    // return await Participants.find({})
  },

  findByCouponcodes: async (params) => {
    /*
      Doc:
        get participant by coupon code
        - In use @WP for apply coupon code
    */
    const populate = Couponcode.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias)
      .join(' ');
    return await Couponcode.find(params).populate(populate);
  }
};
