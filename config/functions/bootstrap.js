'use strict';

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 */

var Users = {}

// import socket io
var io = require('socket.io')(strapi.server);
// listen for user connection

io.on('connection', function (socket) {

  strapi.socket = socket
  strapi.io = io

  // console.log("Bootstrap.js called !!!")

  // Freeze a Given Users Exam
  socket.on('FreezeExam', (value) => {
    console.log(value, "Freeze")
    if (Users.hasOwnProperty(value.Id)) {
      io.to(Users[value.Id].id).emit('FreezeUser');
    }
  })

  //UnFreeze a Given User's Exam
  socket.on('UnFreezeExam', (value) => {

    console.log(value, "Unfreeze")
    if (Users.hasOwnProperty(value.Id)) {
      io.to(Users[value.Id].id).emit('UnFreezeUser');
    }
  })

  socket.on('EndExamNow', (value) => {

    console.log(value, "EndExam")
    if (Users.hasOwnProperty(value.Id)) {
      io.to(Users[value.Id].id).emit('ForceExamEnd');
    }
  })

  socket.on('startExamTestSeries', (value) => {

    // console.log(value, "startExamTestSeries")
    let data = {

      time: value.timeGiven,
      testId: value.testId

    }
    let partiArray = value.participantsArray;
    // console.log(partiArray)
    partiArray.forEach((ParticipantsId) => {
      let userSocket = Users[ParticipantsId]
      if (userSocket) {
        userSocket.join(value.testId)
        io.to(Users[ParticipantsId].id).emit('testSeriesStartExam', data);
      }
    })
    // console.log(partiArray.length)

    // socket.to(value.testId).emit('testSeriesStartExam', data)
  })


  // Get a Users Mongo ID
  socket.on("RegisterUser", (value) => {
    console.log("user Registered ! ", value.MongoId)
    Users[value.MongoId] = socket
  })
});
strapi.io = io; // register socket io inside strapi main object to use it globally anywhere

// socket.emit('hello', JSON.stringify({ message: 'Hello..' }));
// listen for user diconnect
// socket.on('disconnect', () => console.log('a user disconnected'));